# dotnet booking airlines microservices



## STEP 1

-Options: make sure that you create network .. EXTERNAL

## STEP 2

-Options: After clone this https://gitlab.com/yoncedang/dotnet-booking-airlines-microservices

-Options 1: If you running in docker localhost (your computer) .. please change (HOST, PORT) in ocelot.json
    do not using HOST="host.docker.internal"
    do not using PORT=8101 or container port

    MUST USING
    HOST:"containername"
    PORT: 80

-Options 2: If you running in docker server (server (ubuntu, ...)) .. please change (HOST, PORT) in ocelot.json
    do not using HOST="containername"
    do not using PORT=80

    MUST USING
    HOST="host.docker.internal"
    PORT=8101 or your port

## STEP 2
-Options: After clone this https://gitlab.com/yoncedang/nodejs-booking-airlines-microservices


-Options 1: read the file .env, to run create TOPIC

-Options 2: cd to the main project ... read .env.example and create nano .env, after that write like .env.example your secret

## STEP 3


-Options: STOP all container services not include Redis, Postgres ... just services container


-Options: After that .. restart all services container ...
