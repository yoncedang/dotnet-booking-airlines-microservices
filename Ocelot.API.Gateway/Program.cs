using Ocelot.API.Gateway.Extentions;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
builder.Host.AddJsonConfiguration();
// builder.WebHost.HttpsConfiguration();

builder.Services.AddJwtBearer(builder.Configuration);
builder.Services.AddConfigurationSettings(builder.Configuration);
builder.Services.ConfigurationOcelot(builder.Configuration);
builder.Services.ConfigurationCors(builder.Configuration);
builder.Services.SwaggerConfiguration();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    // app.UseSwagger();
    // app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{builder.Environment.ApplicationName} v1"));
}
else
{
    app.UseHttpsRedirection();
}

app.UseCors("CorsPolicy");
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();


app.MapControllers();

app.MapGet("/", context =>
{
    // redirect to swagger
    context.Response.Redirect("/swagger/index.html");

    return Task.CompletedTask;
});

app.UseSwaggerForOcelotUI(options => { options.PathToSwaggerGenerator = "/swagger/docs"; });
await app.UseOcelot();

app.Run();