namespace Ocelot.API.Gateway.Extentions;

public static class JsonConfiguration
{
    public static void AddJsonConfiguration(this ConfigureHostBuilder hostBuilder)
    {
        hostBuilder.ConfigureAppConfiguration((hostingContext, config) =>
        {
            var env = hostingContext.HostingEnvironment;
            config.AddJsonFile("ocelot.json", false, true)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", false, true)
                .AddEnvironmentVariables();
        });
    }

    public static void HttpsConfiguration(this ConfigureWebHostBuilder hostBuilder)
    {
        hostBuilder.UseKestrel(options =>
        {
            var certificatePath = Path.Combine("certificates", "backendmicro.pfx");
            options.ListenAnyIP(443,
                listenOptions => { listenOptions.UseHttps(certificatePath, "181199"); });

            options.ListenAnyIP(80);
        });
    }
}