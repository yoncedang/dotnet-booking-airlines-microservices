using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Identity.Services.ApplicationDB;

// ApplicationContext.cs
public class ApplicationContext : IdentityDbContext<ApplicationUser>
{
    private readonly IdGenerator _idGenerator;


    public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
    {
        _idGenerator = new IdGenerator();
    }

    // public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        SeedAdmin(builder);
        SeedRoles(builder);
        SeedAdminRoles(builder);
    }

    private void SeedAdmin(ModelBuilder builder)
    {
        var admin = new ApplicationUser
        {
            Id = _idGenerator.AdminID,
            UserName = "Admin",
            NormalizedUserName = "ADMIN",
            Email = "huydang.dev@gmail.com",
            NormalizedEmail = "HUYDANG.DEV@GMAIL.COM",
            EmailConfirmed = true,
            FirstName = "Yonce",
            LastName = "Dang",
            LockoutEnabled = true
        };
        var passwordHasher = new PasswordHasher<ApplicationUser>();
        admin.PasswordHash = passwordHasher.HashPassword(admin, "Abc@123456");

        builder.Entity<ApplicationUser>().HasData(admin);
    }

    private void SeedRoles(ModelBuilder builder)
    {
        builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = _idGenerator.RoleID_V1,
                Name = "Admin",
                NormalizedName = "ADMIN",
                ConcurrencyStamp = 1.ToString()
            },
            new IdentityRole
            {
                Id = _idGenerator.ManagerID,
                Name = "Manager",
                NormalizedName = "MANAGER",
                ConcurrencyStamp = 2.ToString()
            },
            new IdentityRole
            {
                Id = _idGenerator.RoleID_V2,
                Name = "User",
                NormalizedName = "USER",
                ConcurrencyStamp = 3.ToString()
            },
            new IdentityRole
            {
                Id = _idGenerator.RoleID_V3,
                Name = "VNA",
                NormalizedName = "VNA",
                ConcurrencyStamp = 4.ToString()
            },
            new IdentityRole
            {
                Id = _idGenerator.RoleID_V4,
                Name = "Bamboo",
                NormalizedName = "BAMBOO",
                ConcurrencyStamp = 5.ToString()
            }
        );
    }

    private void SeedAdminRoles(ModelBuilder builder)
    {
        builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = _idGenerator.RoleID_V1,
                UserId = _idGenerator.AdminID
            }
        );
    }
}