using Microsoft.AspNetCore.Identity;

namespace Identity.Services.ApplicationDB;

public class ApplicationUser : IdentityUser
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime? DateOfBirth { get; set; }

    public string? No { get; set; }
    public string? Street { get; set; }

    public string? Wards { get; set; }

    public string? District { get; set; }

    public string? CityOrProvince { get; set; }
    public string Country { get; set; } = "Vietnamese";

    public string? Image { get; set; }

    public string? publicId { get; set; }
}