namespace Identity.Services.ApplicationDB;

public class IdGenerator
{
    public string AdminID = "d3efca3c-9aa6-4a6d-9038-f4ed53d854dc";
    public string ManagerID = "a7e621bf-31f4-47aa-89a4-e23c607ce46f";
    public string RoleID_V1 = "ada8855a-d60e-48be-9a12-f926040e2e9e";
    public string RoleID_V2 = "8e6d72be-1d76-4f9b-891f-ae14eb8270ee";
    public string RoleID_V3 = "1cd46694-e875-42f5-b586-4ed2bbdb9ef4";
    public string RoleID_V4 = "bbf66b40-81eb-4989-b472-cf0ae6a61381";
}