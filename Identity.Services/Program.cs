using Identity.Services.Startup;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
// builder.WebHost.HttpsBackendmicro();
// swagge


// ConfigProject.cs
new Startup(builder.Services, builder.Configuration);

// RepositoriesConfig.cs
new RepositoriesConfig(builder.Services);

// RedisConfig.cs
new RedisConfig(builder.Services, builder.Configuration);


var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseCors("Developer");
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseCors("Production");
    app.UseHttpsRedirection();
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseStaticFiles();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.MapGet("/", context =>
{
    // redirect to swagger
    context.Response.Redirect("/swagger/index.html");

    return Task.CompletedTask;
});

app.Run();