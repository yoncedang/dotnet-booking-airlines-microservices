using System.Text;
using CloudinaryDotNet;
using Identity.Services.ApacheKafka;
using Identity.Services.ApplicationDB;
using Identity.Services.Helper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace Identity.Services.Startup;

public class Startup
{
    private readonly IConfiguration _configuration;
    private readonly RoleManager<IdentityRole> _roleManager;

    private readonly IServiceCollection _services;
    private readonly UserManager<ApplicationUser> _userManager;

    public Startup(IServiceCollection services, IConfiguration configuration)
    {
        _services = services;
        _configuration = configuration;

        ConfigCors();
        ConfigDbContext();
        Migration();
        ConfigIdentity();
        ConfigJwt();
        ConfigSwagger();
        RegisterKafkaProducer();
        RegisterMapper();
        AddServices();
        Cloudinary();
        HttpsRedirection();
    }


    private void ConfigCors()
    {
        _services.AddCors(options =>
        {
            options.AddPolicy("Developer", builder =>
            {
                builder.WithOrigins("http://localhost:8100")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });
            options.AddPolicy("Production", builder =>
            {
                builder.WithOrigins("https://localhost:8101")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });
        });
    }

    // add dbcontext
    private void ConfigDbContext()
    {
        _services.AddDbContext<ApplicationContext>(options =>
        {
            options.UseSqlServer(_configuration.GetConnectionString("IdentityDB"));
        });

        if (_services.BuildServiceProvider().GetService<ApplicationContext>() is not null)
            Console.WriteLine("IdentityDB connected");

        else
            Console.WriteLine("IdentityDB not connected");
    }

    private void Migration()
    {
        using var scope = _services.BuildServiceProvider().CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<ApplicationContext>();
        context.Database.Migrate();
    }

    // Add auIdentity
    private void ConfigIdentity()
    {
        _services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                options.SignIn.RequireConfirmedEmail = true;
                options.SignIn.RequireConfirmedAccount = true;


                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;


                options.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                options.User.RequireUniqueEmail = true;
            }).AddEntityFrameworkStores<ApplicationContext>()
            .AddDefaultTokenProviders();
    }

    private void HttpsRedirection()
    {
        _services.AddHttpsRedirection(options =>
        {
            options.HttpsPort = 443;
            options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
        });
    }


    // Add Jwt
    private void ConfigJwt()
    {
        _services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateIssuerSigningKey = true,
                ValidAudience = _configuration["JWT:ValidAudience"],
                ValidIssuer = _configuration["JWT:ValidIssuer"],
                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:IssuerSigningKey"])),
                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true,
                RequireExpirationTime = true
            };
        });
    }


    private void ConfigSwagger()
    {
        _services.AddSwaggerGen(option =>
        {
            option.SwaggerDoc("v1", new OpenApiInfo { Title = "Identity.Services", Version = "v1" });

            option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please enter a valid token",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "Bearer"
            });
            option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
        });
    }

    private void RegisterKafkaProducer()
    {
        _services.AddSingleton<KafkaProducerConfiguration>();
    }

    private void RegisterMapper()
    {
        _services.AddAutoMapper(typeof(Helper.AutoMapper));
    }

    private void AddServices()
    {
        _services.AddScoped<LogicHelper>();
    }

    private void Cloudinary()
    {
        _services.AddSingleton(new Cloudinary(new Account(
            _configuration["Cloudinary:CloudName"],
            _configuration["Cloudinary:ApiKey"],
            _configuration["Cloudinary:ApiSecret"]
        )));
    }
}