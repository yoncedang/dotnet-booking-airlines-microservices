using StackExchange.Redis;

namespace Identity.Services.Startup;

public class RedisConfig
{
    private readonly IConfiguration _configuration;

    private readonly IServiceCollection _services;

    public RedisConfig(IServiceCollection services, IConfiguration configuration)
    {
        _services = services;
        _configuration = configuration;
        RedisConnect();
    }

    private void RedisConnect()
    {
        var redis = ConnectionMultiplexer.Connect(_configuration["Redis:Connection"]);
        _services.AddSingleton<IConnectionMultiplexer>(redis);
        Console.WriteLine($"Redis is connected: {redis.IsConnected}");
    }
}