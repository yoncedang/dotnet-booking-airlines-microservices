using Identity.Services.Repositories.IdentityRepo;
using Identity.Services.Repositories.RedisRepo;
using Identity.Services.Repositories.SecurityRepo;
using Identity.Services.Repositories.UserRepo;

namespace Identity.Services.Startup;

public class RepositoriesConfig
{
    private readonly IServiceCollection _services;


    public RepositoriesConfig(IServiceCollection services)
    {
        _services = services;
        RegisterRepositories();
    }

    private void RegisterRepositories()
    {
        _services.AddScoped<IRedisRepositories, RedisRepositories>();
        _services.AddScoped<IUserRepositories, UserRepositories>();
        _services.AddScoped<IIdentityRepositories, IdentityRepositories>();
        _services.AddScoped<ISecurityRepositories, SecurityRepositories>();
    }
}