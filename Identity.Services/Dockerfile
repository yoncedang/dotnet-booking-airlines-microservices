﻿FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER $APP_UID
WORKDIR /app

EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["Identity.Services/Identity.Services.csproj", "Identity.Services/"]
RUN dotnet restore "Identity.Services/Identity.Services.csproj"
COPY . .
WORKDIR "/src/Identity.Services"
RUN dotnet build "Identity.Services.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "Identity.Services.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

RUN mkdir -p /app/certificates
COPY Identity.Services/certificates/backendmicro.pfx /app/certificates
#COPY Identity.Services/certificates/aspnetapp.pfx /app/certificates

ENTRYPOINT ["dotnet", "Identity.Services.dll"]
