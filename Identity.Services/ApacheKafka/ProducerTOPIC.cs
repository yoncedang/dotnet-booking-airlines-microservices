namespace Identity.Services.ApacheKafka;

public class ProducerTOPIC
{
    public const string Verification_Register = "VerificationRegister";
    public const string Resend_Verification_Register = "ResendVerificationRegister";
    public const string Request_Reset_Password = "RequestResetPassword";
}