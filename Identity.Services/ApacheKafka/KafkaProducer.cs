using System.Text.Json;
using Confluent.Kafka;

namespace Identity.Services.ApacheKafka;

public class KafkaProducerConfiguration
{
    private readonly IProducer<string, string> _producer;

    public KafkaProducerConfiguration(IConfiguration configuration)
    {
        var config = new ProducerConfig
        {
            BootstrapServers = configuration["Kafka:BootstrapServers"],
            ClientId = "Identity.Services",
            AllowAutoCreateTopics = true,
            EnableIdempotence = true
        };

        _producer = new ProducerBuilder<string, string>(config).Build();
    }

    public async Task SendMessage(string topic, string key, string message)
    {
        try
        {
            var result = await _producer.ProduceAsync(topic, new Message<string, string>
            {
                Key = key,
                Value = message
            });
            // Check Message is sent
            Console.WriteLine($"Message: {message} sent to {result.TopicPartitionOffset}");
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Delivery failed: {e.Error.Reason}");
        }
    }

    public async Task SendMessageGeneric<T>(string topic, string key, T message)
    {
        try
        {
            var data = JsonSerializer.Serialize(message);
            var result = await _producer.ProduceAsync(topic, new Message<string, string>
            {
                Key = key,
                Value = data
            });
            // Check Message is sent
            Console.WriteLine($"Message: {message} sent to {result.TopicPartitionOffset}");
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Delivery failed: {e.Error.Reason}");
        }
    }
}