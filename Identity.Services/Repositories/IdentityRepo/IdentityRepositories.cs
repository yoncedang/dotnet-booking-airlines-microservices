using Identity.Services.ApplicationDB;
using Microsoft.AspNetCore.Identity;

namespace Identity.Services.Repositories.IdentityRepo;

public class IdentityRepositories : IIdentityRepositories
{
    private readonly UserManager<ApplicationUser> _userManager;

    public IdentityRepositories(UserManager<ApplicationUser> userManager)
    {
        _userManager = userManager;
    }

    public async Task<ApplicationUser> FindByEmailAsync(string email)
    {
        return await _userManager.FindByEmailAsync(email);
    }

    public async Task<ApplicationUser> FindByIdAsync(Guid id)
    {
        return await _userManager.FindByIdAsync(id.ToString());
    }

    public async Task<ApplicationUser> FindByUserNameAsync(string userName)
    {
        return await _userManager.FindByNameAsync(userName);
    }
}