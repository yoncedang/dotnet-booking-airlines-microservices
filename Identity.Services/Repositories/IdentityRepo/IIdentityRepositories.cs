using Identity.Services.ApplicationDB;

namespace Identity.Services.Repositories.IdentityRepo;

public interface IIdentityRepositories
{
    // Find user by email
    public Task<ApplicationUser> FindByEmailAsync(string email);
    public Task<ApplicationUser> FindByIdAsync(Guid id);
    public Task<ApplicationUser> FindByUserNameAsync(string userName);
}