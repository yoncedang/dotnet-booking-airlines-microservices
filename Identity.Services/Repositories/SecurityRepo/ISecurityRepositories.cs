using Identity.Services.DTOs;
using Identity.Services.DTOs.GetAll;
using Microsoft.AspNetCore.Identity;

namespace Identity.Services.Repositories.SecurityRepo;

public interface ISecurityRepositories
{
    public Task<IdentityResult> SendRequestResetPassword(ResendVerificationDTO email);

    public Task<IdentityResult> ResetPassword(NewPasswordDTO newPassword, string token);

    // Get all roles
    public Task<IEnumerable<IdentityRole>> GetAllRoles();

    // Get all user by Role = Manager
    public Task<IEnumerable<GetAllUserDTO>> GetAllUserByRole(Guid RoleId);

    // Update Role
    public Task<IdentityResult> UpdateRole(UpdateRoleDTO updateRoleDTO);

    // suspend user
    public Task<IdentityResult> SuspendUser(SuspendUsersDTO listId);

    // unsuspend user
    public Task<IdentityResult> UnSuspendUser(SuspendUsersDTO listId);

    // All user suspended
    public Task<IEnumerable<GetAllUserDTO>> GetAllUserSuspended();
}