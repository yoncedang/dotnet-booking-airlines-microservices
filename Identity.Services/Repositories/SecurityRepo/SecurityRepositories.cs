using Identity.Services.ApacheKafka;
using Identity.Services.ApplicationDB;
using Identity.Services.DTOs;
using Identity.Services.DTOs.GetAll;
using Identity.Services.DTOs.ProducerSending;
using Identity.Services.Repositories.RedisRepo;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Identity.Services.Repositories.SecurityRepo;

public class SecurityRepositories : ISecurityRepositories
{
    private readonly IConfiguration _configuration;
    private readonly string _key;
    private readonly KafkaProducerConfiguration _producer;
    private readonly IRedisRepositories _redisRepositories;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public SecurityRepositories(UserManager<ApplicationUser> userManager,
        IRedisRepositories redisRepositories, KafkaProducerConfiguration producer, IConfiguration configuration,
        RoleManager<IdentityRole> roleManager)
    {
        _userManager = userManager;
        _redisRepositories = redisRepositories;
        _producer = producer;
        _key = Guid.NewGuid().ToString();
        _configuration = configuration;
        _roleManager = roleManager;
    }

    public async Task<IdentityResult> SendRequestResetPassword(ResendVerificationDTO email)
    {
        var checkUser = await _userManager.FindByEmailAsync(email.Email);
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "EmailNotFound",
                Description = "Email Not Found"
            });

        var resetToken = await _userManager.GeneratePasswordResetTokenAsync(checkUser);

        await _redisRepositories.SetAsync<string>(_key, checkUser.Email);

        await _producer.SendMessageGeneric(ProducerTOPIC.Request_Reset_Password, _key,
            new ResetPasswordDTO
            {
                email = checkUser.Email,
                token = resetToken,
                localhost = _configuration["IpAddress:Localhost"],
                endpoint = _configuration["IpAddress:Endpoint"],
                name = checkUser.FirstName
            });

        return IdentityResult.Success;
    }

    public async Task<IdentityResult> ResetPassword(NewPasswordDTO newPassword, string token)
    {
        var checkToken = await _redisRepositories.GetAsync<VerificationRegisterDTO>(token);
        if (checkToken == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "TokenNotFound",
                Description = "Token Not Match"
            });

        var checkUser = await _userManager.FindByEmailAsync(checkToken.Email);
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "EmailNotFound",
                Description = "Email Not Found"
            });

        var result = await _userManager.ResetPasswordAsync(checkUser, token, newPassword.Password);
        if (!result.Succeeded)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "ResetPasswordFailed",
                Description = "Reset Password Failed"
            });

        await _redisRepositories.DeleteAsync(token);
        return IdentityResult.Success;
    }

    public async Task<IEnumerable<IdentityRole>> GetAllRoles()
    {
        return await _roleManager.Roles.ToListAsync();
    }

    public async Task<IEnumerable<GetAllUserDTO>> GetAllUserByRole(Guid RoleId)
    {
        var checkRole = await _roleManager.FindByIdAsync(RoleId.ToString());
        if (checkRole == null)
            return null;

        var checkUser = await _userManager.GetUsersInRoleAsync(checkRole.Name);
        if (checkUser.Count == 0 || checkUser == null)
            return null;


        var result = new List<GetAllUserDTO>();
        foreach (var user in checkUser)
            result.Add(new GetAllUserDTO
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber,
                DateOfBirth = user.DateOfBirth.ToString(),
                No = user.No,
                Street = user.Street,
                Wards = user.Wards,
                District = user.District,
                CityOrProvince = user.CityOrProvince,
                Country = user.Country,
                Email = user.Email,
                Role = checkRole.Name,
                EmailConfirmed = user.EmailConfirmed.ToString(),
                Image = user.Image
            });

        return result;
    }

    public async Task<IdentityResult> UpdateRole(UpdateRoleDTO updateRoleDto)
    {
        var checkUser = await _userManager.FindByIdAsync(updateRoleDto.UserId.ToString());
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UserNotFound",
                Description = "User Not Found"
            });

        var checkRole = await _roleManager.FindByIdAsync(updateRoleDto.RoleId.ToString());
        if (checkRole == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "RoleNotFound",
                Description = "Role Not Found"
            });

        // delete old role
        var oldRole = await _userManager.GetRolesAsync(checkUser);
        if (oldRole.Count != 0)
            await _userManager.RemoveFromRolesAsync(checkUser, oldRole);

        var result = await _userManager.AddToRoleAsync(checkUser, checkRole.Name);

        if (!result.Succeeded)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "RoleAlreadyExistsBefore",
                Description = "Role Already Exists Before"
            });

        return IdentityResult.Success;
    }

    public async Task<IdentityResult> SuspendUser(SuspendUsersDTO listId)
    {
        if (listId.Id.Count == 0)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "IdNotFound",
                Description = "Id Not Found"
            });

        foreach (var id in listId.Id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {
                await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);
                await _userManager.UpdateSecurityStampAsync(user);

                return IdentityResult.Success;
            }
        }

        return IdentityResult.Failed(new IdentityError
        {
            Code = "SuspendUserFailed",
            Description = "Suspend User Failed"
        });
    }

    public async Task<IdentityResult> UnSuspendUser(SuspendUsersDTO listId)
    {
        if (listId.Id.Count == 0)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "IdNotFound",
                Description = "Id Not Found"
            });

        foreach (var id in listId.Id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {
                await _userManager.ResetAccessFailedCountAsync(user);
                await _userManager.SetLockoutEndDateAsync(user, null);
                await _userManager.UpdateSecurityStampAsync(user);

                return IdentityResult.Success;
            }
        }

        return IdentityResult.Failed(new IdentityError
        {
            Code = "UnSuspendUserFailed",
            Description = "UnSuspend User Failed"
        });
    }

    public async Task<IEnumerable<GetAllUserDTO>> GetAllUserSuspended()
    {
        // Get all user suspended
        var checkUser = await _userManager.Users.ToListAsync();
        if (checkUser.Count == 0 || checkUser == null)
            return null;

        var result = new List<GetAllUserDTO>();
        foreach (var user in checkUser)
            if (user.LockoutEnd != null && user.LockoutEnd == DateTimeOffset.MaxValue)
                result.Add(new GetAllUserDTO
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName,
                    PhoneNumber = user.PhoneNumber,
                    DateOfBirth = user.DateOfBirth.ToString(),
                    No = user.No,
                    Street = user.Street,
                    Wards = user.Wards,
                    District = user.District,
                    CityOrProvince = user.CityOrProvince,
                    Country = user.Country,
                    Email = user.Email,
                    Role = (await _userManager.GetRolesAsync(user)).FirstOrDefault(),
                    EmailConfirmed = user.EmailConfirmed.ToString(),
                    Image = user.Image
                });

        return result;
    }
}