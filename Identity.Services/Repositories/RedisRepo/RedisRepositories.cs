using System.Text.Json;
using StackExchange.Redis;

namespace Identity.Services.Repositories.RedisRepo;

public class RedisRepositories : IRedisRepositories
{
    private readonly IConnectionMultiplexer _redis;

    public RedisRepositories(IConnectionMultiplexer redis)
    {
        _redis = redis;
    }

    public async Task<T> GetAsync<T>(string key)
    {
        try
        {
            var db = _redis.GetDatabase();
            var data = await db.StringGetAsync(key);
            Console.WriteLine(data);
            if (data.IsNullOrEmpty) return default;

            // Deserialize data => Trả về kiểu JSON
            var result = JsonSerializer.Deserialize<T>(data);
            Console.WriteLine(result);
            return result;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return default;
        }
    }

    public async Task SetAsync<T>(string key, T? value, TimeSpan? expiry = null)
    {
        try
        {
            var db = _redis.GetDatabase();

            // Serialize data => Lưu dưới dạng Chuỗi String
            var data = JsonSerializer.Serialize(value);
            await db.StringSetAsync(key, data, expiry);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public async Task<bool> DeleteAsync(string key)
    {
        try
        {
            var db = _redis.GetDatabase();
            var check = db.KeyExists(key);
            if (check) return await db.KeyDeleteAsync(key);

            return false;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }
}