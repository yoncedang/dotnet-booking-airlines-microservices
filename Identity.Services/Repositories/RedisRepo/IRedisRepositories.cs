namespace Identity.Services.Repositories.RedisRepo;

public interface IRedisRepositories
{
    public Task<T> GetAsync<T>(string key);
    public Task SetAsync<T>(string key, T value, TimeSpan? expiry = null);
    public Task<bool> DeleteAsync(string key);
}