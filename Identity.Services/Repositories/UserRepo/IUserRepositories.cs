using Identity.Services.ApplicationDB;
using Identity.Services.DTOs;
using Identity.Services.DTOs.Create;
using Identity.Services.DTOs.GetAll;
using Identity.Services.DTOs.GetById;
using Identity.Services.DTOs.Update;
using Microsoft.AspNetCore.Identity;

namespace Identity.Services.Repositories.UserRepo;

public interface IUserRepositories
{
    public Task<IdentityResult> CreateUserAsync(CreateDTO user);

    // Delete Account
    public Task<IdentityResult> DeleteUserAsync(Guid id);

    // Get All Users
    public Task<IEnumerable<GetAllUserDTO>> GetAllUsersAsync();

    // Get User By Id
    public Task<GetUserIdDTO> GetUserByIdAsync(Guid id);

    // Update User
    public Task<IdentityResult> UpdateUserAsync(Guid id, UpdateUserDTO user);

    // login
    public Task<SignInResult> LoginAsync(ApplicationUser user, LoginDTO login);

    // change password
    public Task<IdentityResult> ChangePasswordAsync(Guid id, ChangePasswordDTO changePassword);

    public Task<IdentityResult> ConfirmEmailRegisterAsync(ConfirmDTO OTP);


    public Task<IdentityResult> ResendVerifyEmailAsync(ResendVerificationDTO email);


    public Task<IdentityResult> UpdateImageAsync(ApplicationUser user, IFormFile image);

    public Task<IEnumerable<GetAllUserDTO>> GetAllManager();
}