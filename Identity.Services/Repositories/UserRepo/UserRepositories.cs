using System.Text.Json;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Identity.Services.ApacheKafka;
using Identity.Services.ApplicationDB;
using Identity.Services.DTOs;
using Identity.Services.DTOs.Create;
using Identity.Services.DTOs.GetAll;
using Identity.Services.DTOs.GetById;
using Identity.Services.DTOs.IdentityRole;
using Identity.Services.DTOs.Update;
using Identity.Services.Helper;
using Identity.Services.Repositories.RedisRepo;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Random = System.Random;

namespace Identity.Services.Repositories.UserRepo;

public class UserRepositories : IUserRepositories
{
    private readonly Cloudinary _cloudinary;
    private readonly IWebHostEnvironment _env;
    private readonly IConfiguration _iconfiguration;
    private readonly string _key;
    private readonly IMapper _mapper;
    private readonly KafkaProducerConfiguration _producer;
    private readonly IRedisRepositories _redisRepositories;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public UserRepositories(IMapper mapper, UserManager<ApplicationUser> userManager, IWebHostEnvironment env,
        IConfiguration configuration, SignInManager<ApplicationUser> signInManager,
        IRedisRepositories redisRepositories, KafkaProducerConfiguration producer, Cloudinary cloudinary)

    {
        _mapper = mapper;
        _userManager = userManager;
        _env = env;
        _iconfiguration = configuration;
        _signInManager = signInManager;
        _redisRepositories = redisRepositories;
        _producer = producer;
        _key = Guid.NewGuid().ToString();
        _cloudinary = cloudinary;
    }

    public async Task<IdentityResult> CreateUserAsync(CreateDTO user)
    {
        var checkEmail = await _userManager.FindByEmailAsync(user.Email);
        var checkUsername = await _userManager.FindByNameAsync(user.UserName);

        var checking = LogicHelper.CheckingEmailAndUserName(checkEmail, checkUsername);
        if (!checking.Succeeded) return checking;

        var mappedUser = _mapper.Map<ApplicationUser>(user);
        var results = await _userManager.CreateAsync(mappedUser, user.Password);
        return results;
    }

    public async Task<IdentityResult> DeleteUserAsync(Guid id)
    {
        var checkUser = await _userManager.FindByIdAsync(id.ToString());
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UserNotFound",
                Description = "User Not Found"
            });
        // Check Image If Exist so delete it
        var wwwrootFolder = Path.Combine(_env.WebRootPath, "Images");
        var checkImage = checkUser.Image;
        if (checkImage != null)
        {
            var uri = new Uri(checkImage);
            var imageName = Path.GetFileName(uri.LocalPath);

            if (File.Exists(Path.Combine(wwwrootFolder, imageName)))
                File.Delete(Path.Combine(wwwrootFolder, imageName));
        }

        return await _userManager.DeleteAsync(checkUser);
    }

    public async Task<IEnumerable<GetAllUserDTO>> GetAllUsersAsync()
    {
        var users = await _userManager.Users.ToListAsync();

        var getAllUserAndRole = new List<GetAllUserDTO>();

        foreach (var user in users)
        {
            var roles = await _userManager.GetRolesAsync(user);
            if (!roles.Contains(RoleIdentity.User)) continue;

            var role = roles.FirstOrDefault();

            var userDto = new GetAllUserDTO
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber,
                DateOfBirth = user.DateOfBirth.ToString(),
                No = user.No,
                Street = user.Street,
                Wards = user.Wards,
                District = user.District,
                CityOrProvince = user.CityOrProvince,
                Country = user.Country,
                EmailConfirmed = user.EmailConfirmed.ToString(),
                Image = user.Image,
                Role = role
            };

            getAllUserAndRole.Add(userDto);
        }

        return getAllUserAndRole;
    }

    public async Task<GetUserIdDTO> GetUserByIdAsync(Guid id)
    {
        var checkUser = await _userManager.FindByIdAsync(id.ToString());
        if (checkUser == null) return null;
        var checkRole = (await _userManager.GetRolesAsync(checkUser)).FirstOrDefault();
        var user = new GetUserIdDTO
        {
            Id = checkUser.Id,
            FirstName = checkUser.FirstName,
            LastName = checkUser.LastName,
            Email = checkUser.Email,
            UserName = checkUser.UserName,
            PhoneNumber = checkUser.PhoneNumber,
            DateOfBirth = checkUser.DateOfBirth.ToString(),
            No = checkUser.No,
            Street = checkUser.Street,
            Wards = checkUser.Wards,
            District = checkUser.District,
            CityOrProvince = checkUser.CityOrProvince,
            Country = checkUser.Country,
            EmailConfirmed = checkUser.EmailConfirmed.ToString(),
            Image = checkUser.Image,
            Role = checkRole
        };
        return user;
    }

    public async Task<SignInResult> LoginAsync(ApplicationUser user, LoginDTO login)
    {
        var result = await _signInManager.PasswordSignInAsync(user, login.Password, false, true);

        // check suspended account

        // if lockout
        if (result.IsLockedOut)
            return SignInResult.LockedOut;

        if (result.Succeeded)
            return SignInResult.Success;

        if (result.IsNotAllowed) return SignInResult.NotAllowed;

        return SignInResult.Failed;
    }

    public async Task<IdentityResult> ChangePasswordAsync(Guid id, ChangePasswordDTO changePassword)
    {
        var checkUser = await _userManager.FindByIdAsync(id.ToString());
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UserNotFound",
                Description = "User Not Found"
            });

        var checkPassword = await _userManager.CheckPasswordAsync(checkUser, changePassword.OldPassword);
        if (!checkPassword)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "WrongPassword",
                Description = "Wrong Password"
            });
        var result = await _userManager.ChangePasswordAsync(checkUser, changePassword.OldPassword,
            changePassword.NewPassword);

        return result;
    }

    public async Task<IdentityResult> ConfirmEmailRegisterAsync(ConfirmDTO confirmDto)
    {
        var checkOTP = _redisRepositories.GetAsync<VerificationRegisterDTO>(confirmDto.OTP).Result;
        if (checkOTP == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "OTPNotFound",
                Description = "OTP Not Found or Expired"
            });

        var checkUser = await _userManager.FindByEmailAsync(checkOTP.Email);
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UserNotFound",
                Description = "User Not Found"
            });

        checkUser.EmailConfirmed = true;
        var result = await _userManager.UpdateAsync(checkUser);
        if (result.Succeeded)
            await _redisRepositories.DeleteAsync(confirmDto.OTP);

        return result;
    }

    public async Task<IdentityResult> ResendVerifyEmailAsync(ResendVerificationDTO email)
    {
        var checkUser = await _userManager.FindByEmailAsync(email.Email);
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "EmailNotFound",
                Description = "Email Not Found"
            });

        if (checkUser.EmailConfirmed)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "EmailConfirmed",
                Description = "Email already confirmed - Please login"
            });

        // Khong can Await vi khong can tra ve ket qua
        _redisRepositories.SetAsync<string>(_key, checkUser.Email);

        _producer.SendMessage(ProducerTOPIC.Resend_Verification_Register, _key, JsonSerializer.Serialize(new
        {
            email = checkUser.Email,
            code = new Random().Next(100000, 1000000),
            name = checkUser.FirstName + " " + checkUser.LastName
        }));

        return IdentityResult.Success;
    }

    public async Task<IdentityResult> UpdateUserAsync(Guid id, UpdateUserDTO user)
    {
        var checkUser = await _userManager.FindByIdAsync(id.ToString());
        if (checkUser == null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UserNotFound",
                Description = "User Not Found"
            });
        LogicHelper.UpdateUser(checkUser, user);
        var result = await _userManager.UpdateAsync(checkUser);
        return result;
    }

    public async Task<IdentityResult> UpdateImageAsync(ApplicationUser user, IFormFile image)
    {
        // Just allow upload image ContentType is Image
        if (!image.ContentType.Contains("image"))
            return IdentityResult.Failed(new IdentityError
            {
                Code = "JustAllowUploadImage",
                Description = "Just Allow Upload Image"
            });

        var fileName = _key + Path.GetExtension(image.FileName);
        // Upload to cloudinary
        // Check Image If Exist so delete it Cloudinary
        var publicId = user.publicId;
        var stream = image.OpenReadStream();

        if (publicId != null)
        {
            var deleteParams = new DeletionParams(publicId);
            await _cloudinary.DestroyAsync(deleteParams);
        }

        var uploadParams = new ImageUploadParams
        {
            File = new FileDescription(fileName, stream),
            Folder = "Images"
        };

        var uploadResult = await _cloudinary.UploadAsync(uploadParams);
        if (uploadResult.Error != null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UploadImageFailed",
                Description = "Upload Image Failed"
            });

        user.Image = uploadResult.SecureUrl.AbsoluteUri;
        user.publicId = uploadResult.PublicId;


        var result = await _userManager.UpdateAsync(user);

        return result;
    }

    public async Task<IEnumerable<GetAllUserDTO>> GetAllManager()
    {
        var managers = await _userManager.Users.ToListAsync();

        var getAllManager = new List<GetAllUserDTO>();

        foreach (var manager in managers)
        {
            var roles = await _userManager.GetRolesAsync(manager);
            if (roles.Contains(RoleIdentity.User) || roles.Contains(RoleIdentity.Admin)) continue;

            var role = roles.FirstOrDefault();

            var managerDto = new GetAllUserDTO
            {
                Id = manager.Id,
                FirstName = manager.FirstName,
                LastName = manager.LastName,
                Email = manager.Email,
                UserName = manager.UserName,
                PhoneNumber = manager.PhoneNumber,
                DateOfBirth = manager.DateOfBirth.ToString(),
                No = manager.No,
                Street = manager.Street,
                Wards = manager.Wards,
                District = manager.District,
                CityOrProvince = manager.CityOrProvince,
                Country = manager.Country,
                EmailConfirmed = manager.EmailConfirmed.ToString(),
                Image = manager.Image,
                Role = role
            };

            getAllManager.Add(managerDto);
        }

        return getAllManager;
    }
}