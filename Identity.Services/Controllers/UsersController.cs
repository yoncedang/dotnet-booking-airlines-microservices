using System.Security.Claims;
using System.Text.Json;
using Identity.Services.ApacheKafka;
using Identity.Services.ApplicationDB;
using Identity.Services.DTOs;
using Identity.Services.DTOs.Create;
using Identity.Services.DTOs.IdentityRole;
using Identity.Services.DTOs.Update;
using Identity.Services.Helper;
using Identity.Services.Repositories.IdentityRepo;
using Identity.Services.Repositories.RedisRepo;
using Identity.Services.Repositories.UserRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class authController : ControllerBase
{
    private readonly IIdentityRepositories _identityRepositories;
    private readonly string _key;
    private readonly LogicHelper _logicHelper;
    private readonly KafkaProducerConfiguration _producer;
    private readonly IRedisRepositories _redis;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IUserRepositories _userRepositories;

    public authController(IUserRepositories userRepositories, IIdentityRepositories identityRepositories,
        RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager,
        KafkaProducerConfiguration producer, IRedisRepositories redis, LogicHelper logicHelper)
    {
        _userRepositories = userRepositories;
        _identityRepositories = identityRepositories;
        _roleManager = roleManager;
        _userManager = userManager;
        _producer = producer;
        _redis = redis;
        _key = Guid.NewGuid().ToString();

        _logicHelper = logicHelper;
    }

    [AllowAnonymous]
    [HttpGet("admin-account")]
    public async Task<IActionResult> AdminAccount()
    {
        try {
            return Ok(new
                {
                    message = "Get success account Admin management system",
                    data = new
                    {
                        email = "dev.huydang@gmail.com",
                        username = "admin",
                        password = "Abc@123456"
                    }
                });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<IActionResult> Register(CreateDTO user)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _userRepositories.CreateUserAsync(user);
            if (result.Succeeded)
            {
                var currentUser = await _identityRepositories.FindByEmailAsync(user.Email);

                // Dont need to await
                await _redis.SetAsync(_key, JsonSerializer.Serialize(new
                {
                    email = currentUser.Email
                }));

                // Dont need to await
                await _producer.SendMessage(ProducerTOPIC.Verification_Register, _key,
                    JsonSerializer.Serialize(new
                    {
                        email = currentUser.Email,
                        name = currentUser.FirstName + " " + currentUser.LastName,
                        code = new Random().Next(100000, 1000000)
                    }));


                if (!await _roleManager.RoleExistsAsync(RoleIdentity.User))
                {
                    Console.WriteLine("Adding new role");
                    var role = new IdentityRole(RoleIdentity.User);
                    await _roleManager.CreateAsync(role);
                }

                await _userManager.AddToRoleAsync(currentUser, RoleIdentity.User);

                return Ok(new
                {
                    message = $"Please check your email {user.Email.ToUpper()} to verify your account",
                    data = new
                    {
                        email = currentUser.Email,
                        username = currentUser.UserName,
                        role = RoleIdentity.User,
                        EmailConfirm = currentUser.EmailConfirmed
                    }
                });
            }

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [AllowAnonymous]
    [HttpPost("resend-verification")]
    public async Task<IActionResult> ResendVerification(ResendVerificationDTO email)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _userRepositories.ResendVerifyEmailAsync(email);
            if (!result.Succeeded)
                return BadRequest(new
                {
                    message = result.Errors
                });

            return Ok(new
            {
                message = $"An OTP has been sent to your email {email.Email.ToUpper()}"
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [AllowAnonymous]
    [HttpPost("confirm-register")]
    public async Task<IActionResult> ConfirmRegister(ConfirmDTO OTP)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _userRepositories.ConfirmEmailRegisterAsync(OTP);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Welcome to our website",
                    confirm = true
                });

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    // login with email or username
    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginDTO login)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values);
            var checkUser = await _logicHelper.CheckUser(login);
            if (checkUser == null)
                return BadRequest(new { message = "User not found" });

            if (checkUser.LockoutEnd != null && checkUser.LockoutEnd == DateTimeOffset.MaxValue)
                return BadRequest(new
                {
                    message = "Your account has been locked out by Admin. Please contact"
                });

            var result = await _userRepositories.LoginAsync(checkUser, login);
            if (result.IsLockedOut)
            {
                // Tài khoản bị khoá, xử lý tại đây nếu cần
                var lockoutEndDate = await _userManager.GetLockoutEndDateAsync(checkUser);
                var remainingLockoutTime = lockoutEndDate - DateTimeOffset.Now;

                return BadRequest(
                    new
                    {
                        time = lockoutEndDate,
                        remain = DateTimeOffset.Now,
                        message = "Your account has been locked out. Please try after " +
                                  remainingLockoutTime.Value.Minutes + " minutes" + " and " +
                                  remainingLockoutTime.Value.Seconds +
                                  " seconds",
                        ResetPassword = "If you forgot your password, please click forgot password below"
                    });
            }

            if (result.Succeeded)
            {
                var token = _logicHelper.GenerateToken(checkUser);
                return Ok(new
                {
                    message = "Login successfully",
                    data = new
                    {
                        token = token.Token,
                        ExpireDate = token.Expiration
                    }
                });
            }


            if (result.IsNotAllowed)
                return BadRequest(new { message = "Your account still verify Email or has been suspended by Admin" });

            return BadRequest(new { message = "Wrong password" });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _userRepositories.DeleteUserAsync(id);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "User deleted successfully"
                });

            return BadRequest(new
            {
                result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }


    [Authorize(Roles = "Admin, Manager")]
    [HttpGet("all-users")]
    public async Task<IActionResult> GetAll(int page, int Size)
    {
        try
        {
            var result = await _userRepositories.GetAllUsersAsync();
            return Ok(new
            {
                message = "Get all users successfully",
                data = new
                {
                    currentPage = page,
                    totalPage = Math.Ceiling((double)result.Count() / Size),
                    users = result.Skip((page - 1) * Size).Take(Size)
                }
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin, Manager")]
    [HttpGet("all-manager")]
    public async Task<IActionResult> GetAllManager()
    {
        try
        {
            var result = await _userRepositories.GetAllManager();
            return Ok(new
            {
                message = "Get all manager successfully",
                data = result
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message
            });
        }
    }


    [Authorize(Roles = "Admin, Manager")]
    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(Guid id)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _userRepositories.GetUserByIdAsync(id);
            if (result != null)
                return Ok(new
                {
                    message = "Get user by id successfully",
                    data = result
                });

            return BadRequest(new
            {
                message = "Error while getting user by id"
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize]
    [HttpPut("update-profile")]
    public async Task<IActionResult> Update(UpdateUserDTO user)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var id = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            if (id == null)
                return Unauthorized(new
                {
                    message = "Token is invalid"
                });

            var result = await _userRepositories.UpdateUserAsync(id, user);
            if (result.Succeeded)
                return Ok(new
                {
                    message = true
                });


            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize]
    [HttpPut("change-password")]
    public async Task<IActionResult> ChangePassword(ChangePasswordDTO changePassword)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var id = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            if (id == null)
                return Unauthorized(new
                {
                    message = "Token is invalid"
                });

            var result = await _userRepositories.ChangePasswordAsync(id, changePassword);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Change password successfully"
                });

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize]
    [HttpPut("update-image")]
    public async Task<IActionResult> UpdateImage(IFormFile image)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var id = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            if (id == null)
                return Unauthorized(new
                {
                    message = "Token is invalid"
                });
            var checkUser = await _identityRepositories.FindByIdAsync(id);
            if (checkUser == null)
                return BadRequest(new
                {
                    message = "User not found"
                });
            var result = await _userRepositories.UpdateImageAsync(checkUser, image);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Update image successfully"
                });

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }
}