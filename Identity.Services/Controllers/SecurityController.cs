using Identity.Services.DTOs;
using Identity.Services.Repositories.SecurityRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class securityController : ControllerBase
{
    private readonly ISecurityRepositories _securityRepositories;

    public securityController(ISecurityRepositories securityRepositories)
    {
        _securityRepositories = securityRepositories;
    }

    [AllowAnonymous]
    [HttpPost("forgot-password")]
    public async Task<IActionResult> ForgotPassword(ResendVerificationDTO email)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _securityRepositories.SendRequestResetPassword(email);
            if (result.Succeeded)
                return Ok(new
                {
                    message = $"An OTP has been sent to your email {email.Email.ToUpper()}"
                });

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [AllowAnonymous]
    [HttpPost("reset-password")]
    public async Task<IActionResult> ResetPassword(NewPasswordDTO newPassword, string token)
    {
        try
        {
            if (!ModelState.IsValid)
                return BadRequest(new
                {
                    ModelState.Values
                });
            var result = await _securityRepositories.ResetPassword(newPassword, token);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Reset Password Successfully"
                });

            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }


    [HttpGet("all-roles")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> GetAllRoles()
    {
        try
        {
            var result = await _securityRepositories.GetAllRoles();
            return Ok(new
            {
                message = "Get All Roles Successfully",
                data = result
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpGet("role/{id}")]
    public async Task<IActionResult> GetAllUserByRole(Guid id)
    {
        try
        {
            var result = await _securityRepositories.GetAllUserByRole(id);
            if (result == null)
                return BadRequest(new
                {
                    message = "Role Not Found or User Not Found"
                });
            return Ok(new
            {
                message = "Get All User By Role Successfully",
                data = result
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut("role")]
    public async Task<IActionResult> UpdateRole(UpdateRoleDTO updateRoleDTO)
    {
        try
        {
            var result = await _securityRepositories.UpdateRole(updateRoleDTO);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Update Role Successfully"
                });
            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut("suspend-user")]
    public async Task<IActionResult> SuspendUser([FromForm] SuspendUsersDTO listId)
    {
        try
        {
            var result = await _securityRepositories.SuspendUser(listId);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "Suspend User Successfully"
                });
            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut("unsuspend-user")]
    public async Task<IActionResult> UnSuspendUser([FromForm] SuspendUsersDTO listId)
    {
        try
        {
            var result = await _securityRepositories.UnSuspendUser(listId);
            if (result.Succeeded)
                return Ok(new
                {
                    message = "UnSuspend User Successfully"
                });
            return BadRequest(new
            {
                message = result.Errors
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpGet("users-suspended")]
    public async Task<IActionResult> GetAllUserSuspended()
    {
        try
        {
            var result = await _securityRepositories.GetAllUserSuspended();
            return Ok(new
            {
                message = "Get All User Suspended Successfully",
                data = result
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                message = e.Message,
                error = false
            });
        }
    }
}