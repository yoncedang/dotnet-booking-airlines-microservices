using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Identity.Services.ApplicationDB;
using Identity.Services.DTOs;
using Identity.Services.DTOs.Update;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace Identity.Services.Helper;

public class LogicHelper
{
    private readonly IConfiguration _configuration;
    private readonly UserManager<ApplicationUser> _userManager;

    public LogicHelper(UserManager<ApplicationUser> userManager, IConfiguration configuration)
    {
        _userManager = userManager;
        _configuration = configuration;
    }

    public static void UpdateUser(ApplicationUser user, UpdateUserDTO updateUser)
    {
        user.FirstName = updateUser.FirstName ?? user.FirstName;
        user.LastName = updateUser.LastName ?? user.LastName;
        user.PhoneNumber = updateUser.PhoneNumber ?? user.PhoneNumber;
        user.No = updateUser.No ?? user.No;
        user.Street = updateUser.Street ?? user.Street;
        user.Wards = updateUser.Wards ?? user.Wards;
        user.District = updateUser.District ?? user.District;
        user.CityOrProvince = updateUser.CityOrProvince ?? user.CityOrProvince;
        user.Country = updateUser.Country ?? user.Country;
        user.DateOfBirth = updateUser.DateOfBirth ?? user.DateOfBirth;
    }


    public static IdentityResult CheckingEmailAndUserName(ApplicationUser email, ApplicationUser username)
    {
        if (email != null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "EmailAlreadyExists",
                Description = "Email already exists"
            });
        if (username != null)
            return IdentityResult.Failed(new IdentityError
            {
                Code = "UsernameAlreadyExists",
                Description = "Username already exists"
            });
        return IdentityResult.Success;
    }

    public async Task<ApplicationUser> CheckUser(LoginDTO login)
    {
        var checkEmail = await _userManager.FindByEmailAsync(login.UserNameOrEmail);
        var checkUsername = await _userManager.FindByNameAsync(login.UserNameOrEmail);
        var user = checkEmail ?? checkUsername;
        if (user == null) return null;
        return user;
    }

    public TokenDTO GenerateToken(ApplicationUser user)
    {
        var TokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.UTF8.GetBytes(_configuration["JWT:IssuerSigningKey"]);
        var roles = _userManager.GetRolesAsync(user).Result.FirstOrDefault();

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Issuer = _configuration["JWT:ValidIssuer"],
            Audience = _configuration["JWT:ValidAudience"],
            Subject = new ClaimsIdentity(new List<Claim>
            {
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new(ClaimTypes.NameIdentifier, user.Id),
                new(ClaimTypes.Role, roles),
                new(ClaimTypes.Email, user.Email),
                new(ClaimTypes.Name, user.UserName)
            }),
            Expires = DateTime.UtcNow.AddDays(7),
            // Expires = DateTime.UtcNow.AddSeconds(30),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };

        var token = TokenHandler.CreateToken(tokenDescriptor);
        var tokenString = TokenHandler.WriteToken(token);

        var tokenDto = new TokenDTO
        {
            Token = tokenString,
            Expiration = token.ValidTo
        };

        return tokenDto;
    }
}