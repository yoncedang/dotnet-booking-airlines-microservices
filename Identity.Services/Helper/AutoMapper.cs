using AutoMapper;
using Identity.Services.ApplicationDB;
using Identity.Services.DTOs.Create;
using Identity.Services.DTOs.GetAll;
using Identity.Services.DTOs.GetById;
using Identity.Services.DTOs.Update;
using Microsoft.AspNetCore.Identity;

namespace Identity.Services.Helper;

public class AutoMapper : Profile
{
    public AutoMapper()
    {
        CreateMap<CreateDTO, ApplicationUser>().ReverseMap();

        CreateMap<GetAllUserDTO, ApplicationUser>().ReverseMap();
        CreateMap<GetAllUserDTO, IdentityRole>().ReverseMap();

        CreateMap<GetUserIdDTO, ApplicationUser>().ReverseMap();
        CreateMap<GetUserIdDTO, IdentityRole>().ReverseMap();

        CreateMap<UpdateUserDTO, ApplicationUser>().ReverseMap();
    }
}