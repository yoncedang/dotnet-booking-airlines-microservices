namespace Identity.Services.DTOs.ProducerSending;

public class ResetPasswordDTO
{
    public string email { get; set; }
    public string token { get; set; }

    public string localhost { get; set; }

    public string endpoint { get; set; }

    public string name { get; set; }
}