using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class ConfirmDTO
{
    [Required(ErrorMessage = "OTP is required")]
    [DataType(DataType.Text)]
    [MinLength(6)]
    [MaxLength(6)]
    public string OTP { get; set; }
}