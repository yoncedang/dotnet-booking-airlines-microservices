using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class ChangePasswordDTO
{
    [Required(ErrorMessage = "Old password is required")]
    [MinLength(8)]
    [MaxLength(50)]
    [DataType(DataType.Password)]
    public string OldPassword { get; set; }


    [MinLength(8)]
    [MaxLength(50)]
    [Required(ErrorMessage = "New password is required")]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; }

    [MinLength(8)]
    [MaxLength(50)]
    [Required(ErrorMessage = "Confirm password is required")]
    [DataType(DataType.Password)]
    [Compare("NewPassword", ErrorMessage = "Confirm password is not match")]
    public string ConfirmPassword { get; set; }
}