namespace Identity.Services.DTOs;

public class VerificationRegisterDTO
{
    public string Email { get; set; }
}