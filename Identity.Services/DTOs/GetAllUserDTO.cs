namespace Identity.Services.DTOs.GetAll;

public class GetAllUserDTO
{
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }

    public string PhoneNumber { get; set; }

    public string DateOfBirth { get; set; }

    public string No { get; set; }

    public string Street { get; set; }

    public string Wards { get; set; }

    public string District { get; set; }

    public string CityOrProvince { get; set; }

    public string Country { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
    public string EmailConfirmed { get; set; }
    public string Image { get; set; }
}