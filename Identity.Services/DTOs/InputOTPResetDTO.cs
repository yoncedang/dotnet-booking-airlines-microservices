namespace Identity.Services.DTOs;

public class InputOTPResetDTO
{
    public string OTP { get; set; }
}