using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class SuspendUsersDTO
{
    [Required(ErrorMessage = "Id is required")]
    public List<Guid> Id { get; set; }
}