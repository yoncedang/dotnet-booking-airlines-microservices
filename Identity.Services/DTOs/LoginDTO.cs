using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class LoginDTO
{
    [Required(ErrorMessage = "Username or email is required")]
    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(5)]

    public string UserNameOrEmail { get; set; }

    [DataType(DataType.Password)]
    [MaxLength(50)]
    [MinLength(8)]
    [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; }
}