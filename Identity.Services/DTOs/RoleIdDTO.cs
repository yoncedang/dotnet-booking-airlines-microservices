namespace Identity.Services.DTOs;

public class RoleIdDTO
{
    public string RoleId { get; set; }
}