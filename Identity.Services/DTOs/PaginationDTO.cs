namespace Identity.Services.DTOs;

public class PaginationDTO
{
    public int Page { get; set; }
    public int RecordsPerPage { get; set; }
}