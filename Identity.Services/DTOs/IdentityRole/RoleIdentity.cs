namespace Identity.Services.DTOs.IdentityRole;

public class RoleIdentity
{
    // User role
    public const string User = "User";
    public const string VNA = "VNA";
    public const string Bamboo = "Bamboo";
    public const string Manager = "Manager";
    public const string Admin = "Admin";
}