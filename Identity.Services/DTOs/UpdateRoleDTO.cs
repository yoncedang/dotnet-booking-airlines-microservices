using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class UpdateRoleDTO
{
    [Required(ErrorMessage = "UserId is required")]
    public Guid UserId { get; set; }

    [Required(ErrorMessage = "RoleId is required")]
    public Guid RoleId { get; set; }
}