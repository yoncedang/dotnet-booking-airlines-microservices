using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class ResendVerificationDTO
{
    [Required(ErrorMessage = "Email is Required")]
    [EmailAddress(ErrorMessage = "Email is not valid")]
    [DataType(DataType.EmailAddress)]
    public string Email { get; set; }
}