using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs;

public class NewPasswordDTO
{
    [Required(ErrorMessage = "Email is required")]
    [DataType(DataType.Password)]
    [MaxLength(50)]
    [MinLength(8)]

    public string Password { get; set; }

    [Required(ErrorMessage = "Confirm Password is required")]
    [DataType(DataType.Password)]
    [MaxLength(50)]
    [MinLength(8)]
    [Compare("Password", ErrorMessage = "Password and Confirm Password must be the same")]
    public string ConfirmPassword { get; set; }
}