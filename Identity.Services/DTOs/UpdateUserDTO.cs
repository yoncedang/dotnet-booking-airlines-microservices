using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs.Update;

public class UpdateUserDTO
{
    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(2)]
    public string? FirstName { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(2)]
    public string? LastName { get; set; }


    [DataType(DataType.Date)] public DateTime? DateOfBirth { get; set; }


    // phone number
    [DataType(DataType.PhoneNumber)]
    [MaxLength(50)]
    [MinLength(10)]
    public string? PhoneNumber { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(10)]
    [MinLength(1)]
    public string? No { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(3)]
    public string? Street { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(3)]
    public string? Wards { get; set; }


    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(3)]
    public string? District { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(3)]
    public string? CityOrProvince { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(3)]
    public string? Country { get; set; }
}