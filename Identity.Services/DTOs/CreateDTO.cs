using System.ComponentModel.DataAnnotations;

namespace Identity.Services.DTOs.Create;

public class CreateDTO
{
    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(2)]
    [Required(ErrorMessage = "First name is required")]
    public string FirstName { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(2)]
    [Required(ErrorMessage = "Last name is required")]
    public string LastName { get; set; }

    [DataType(DataType.Text)]
    [MaxLength(50)]
    [MinLength(5)]
    [Required(ErrorMessage = "Username is required")]
    public string UserName { get; set; }

    [DataType(DataType.EmailAddress)]
    [MaxLength(50)]
    [Required(ErrorMessage = "Email is required")]
    [EmailAddress(ErrorMessage = "Email is not valid")]
    public string Email { get; set; }

    [DataType(DataType.Password)]
    [MaxLength(50)]
    [MinLength(8)]
    [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [MaxLength(50)]
    [MinLength(8)]
    [Required(ErrorMessage = "Confirm password is required")]
    [Compare("Password", ErrorMessage = "Confirm password is not match")]
    public string ConfirmPassword { get; set; }
}