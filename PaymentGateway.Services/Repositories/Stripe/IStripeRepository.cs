using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.Models;
using Stripe.Checkout;

namespace PaymentGateway.Services.Repositories.Stripe;

public interface IStripeRepository
{
    // PaymentWith UserId
    Task<OperationResult<Session>> GetPaymentAsync(BasketInfo basket);
}