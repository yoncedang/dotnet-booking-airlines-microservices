using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.Models;
using Stripe;
using Stripe.Checkout;

// using Stripe.BillingPortal;

namespace PaymentGateway.Services.Repositories.Stripe;

public class StripeRepository : IStripeRepository
{
    private readonly OperationResult<Session> _operationResult;
    private readonly string _stripeSecretKey;

    public StripeRepository(IConfiguration configuration, OperationResult<Session> operationResult)
    {
        _stripeSecretKey = configuration["StripeConfiguration:SecretKey"];
        StripeConfiguration.ApiKey = _stripeSecretKey;
        _operationResult = operationResult;
    }


    public async Task<OperationResult<Session>> GetPaymentAsync(BasketInfo basket)
    {
        try
        {
            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> { "card" },
                LineItems = new List<SessionLineItemOptions>
                {
                    new()
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            Currency = "usd",
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = $"Ticket Payment for {basket.AmountPassenger} passenger(s)"
                            },
                            UnitAmount =
                                (long)basket
                                    .Price * 100 /
                                basket
                                    .AmountTicket
                        },
                        Quantity = basket.AmountTicket
                    }
                },
                Mode = "payment",
                SuccessUrl = "http://backendmicro.me/services/payment/success",
                CancelUrl = "http://backendmicro.me/services/payment/cancel",
                Metadata = new Dictionary<string, string>
                {
                    { "UserId", basket.UserId.ToString() },
                    { "BasketId", basket.BasketId.ToString() },
                    { "Price", basket.Price.ToString() },
                    { "AmountTicket", basket.AmountTicket.ToString() },
                    { "FlightComeId", basket.FlightComeId.ToString() },
                    { "FlightBackId", basket.FlightBackId.ToString() },
                    { "IsPending", basket.isPending.ToString() },
                    { "Status", basket.Status },
                    { "AmountPassenger", basket.AmountPassenger.ToString() }
                }
            };
            options.ExpiresAt = DateTime.UtcNow.AddMinutes(30);
            var service = new SessionService();
            var session = await service.CreateAsync(options);

            return _operationResult.Success("Success", session);
        }
        catch (Exception e)
        {
            Console.WriteLine("Loi day nha: " + e.Message);
            return _operationResult.Failed(e.Message);
        }
    }
}