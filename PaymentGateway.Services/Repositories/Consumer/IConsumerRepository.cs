using PaymentGateway.Services.ApacheKafka.DataReceiveDTO;
using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.Models;

namespace PaymentGateway.Services.Repositories.Consumer;

public interface IConsumerRepository
{
    // check UserId Cookies or add to db
    Task<OperationResult<BasketInfo>> CheckUserId(ReceiveBasketDTO receiveBasketDTO);
}