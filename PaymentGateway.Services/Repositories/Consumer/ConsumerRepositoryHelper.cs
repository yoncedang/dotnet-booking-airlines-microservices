using Microsoft.EntityFrameworkCore;
using PaymentGateway.Services.ApacheKafka.DataReceiveDTO;
using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.DbContextPayment;
using PaymentGateway.Services.Models;

namespace PaymentGateway.Services.Repositories.Consumer;

public class ConsumerRepositoryHelper
{
    private readonly PaymentContextDB _contextDb;
    private readonly OperationResult<BasketInfo> _operationResult;

    public ConsumerRepositoryHelper(PaymentContextDB paymentContextDB, OperationResult<BasketInfo> operationResult)
    {
        _contextDb = paymentContextDB;
        _operationResult = operationResult;
    }

    public async Task<bool> checkBasketClean(Guid id, bool isPending, string status)
    {
        Console.WriteLine("Check Basket Clean");
        var check = await _contextDb.BasketInfos
            .Where(x => x.UserId == id && x.isPending == isPending && x.Status == status).ToListAsync();
        if (check.Count == 0) return true;

        foreach (var basket in check)
        {
            Console.WriteLine("Remove Basket Info");
            _contextDb.BasketInfos.Remove(basket);
        }

        await _contextDb.SaveChangesAsync();

        return true;
    }

    public OperationResult<BasketInfo> CreateBasketInfo(ReceiveBasketDTO receiveBasketDTO)
    {
        Console.WriteLine("Create Basket Info");
        var basketInfo = new BasketInfo
        {
            BasketId = receiveBasketDTO.basket.Id,
            UserId = receiveBasketDTO.basket.UserId,
            AmountPassenger = receiveBasketDTO.basket.AmountPassenger,
            AmountTicket = receiveBasketDTO.basket.AmountTicket,
            Price = receiveBasketDTO.basket.Price,
            AirlinesCome = receiveBasketDTO.basket.AirlinesCome,
            AirlinesBack = receiveBasketDTO.basket.AirlinesBack,
            isPending = receiveBasketDTO.basket.isPending,
            FlightComeId = receiveBasketDTO.basket.FlightComeId,
            FlightBackId = receiveBasketDTO.basket.FlightBackId,
            Status = receiveBasketDTO.basket.Status,
            CreateAt = receiveBasketDTO.basket.CreateAt,
            UpdateAt = receiveBasketDTO.basket.UpdateAt
        };

        return _operationResult.Success("Success", basketInfo);
    }

    // Save BasketInfo to Database
    public async Task<OperationResult<BasketInfo>> SaveBasketInfo(BasketInfo basketInfo)
    {
        Console.WriteLine("Save Basket Info");
        try
        {
            await _contextDb.BasketInfos.AddAsync(basketInfo);
            await _contextDb.SaveChangesAsync();
            return _operationResult.Success("Success", basketInfo);
        }
        catch (Exception e)
        {
            return _operationResult.Failed(e.Message);
        }
    }
}