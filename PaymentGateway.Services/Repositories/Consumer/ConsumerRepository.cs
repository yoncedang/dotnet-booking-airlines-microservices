using PaymentGateway.Services.ApacheKafka.DataReceiveDTO;
using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.Models;
using PaymentGateway.Services.Repositories.Redis;

namespace PaymentGateway.Services.Repositories.Consumer;

public class ConsumerRepository : IConsumerRepository
{
    private readonly ConsumerRepositoryHelper _helper;


    private readonly OperationResult<BasketInfo> _operationResult;
    private readonly IRedisRepositories _redis;

    public ConsumerRepository(OperationResult<BasketInfo> operationResult,
        ConsumerRepositoryHelper consumerRepositoryHelper, IRedisRepositories redis)

    {
        _operationResult = operationResult;
        _helper = consumerRepositoryHelper;
        _redis = redis;
    }

    public async Task<OperationResult<BasketInfo>> CheckUserId(ReceiveBasketDTO receiveBasketDTO)
    {
        try
        {
            if (receiveBasketDTO.role.ToLower() != "user")
                await _redis.SetAsync(receiveBasketDTO.basket.UserId.ToString(), true, TimeSpan.FromHours(12));


            await _helper.checkBasketClean(receiveBasketDTO.basket.UserId,
                receiveBasketDTO.basket.isPending,
                receiveBasketDTO.basket.Status);

            var basketInfo = _helper.CreateBasketInfo(receiveBasketDTO);
            if (basketInfo._isSuccess)
            {
                var results = await _helper.SaveBasketInfo(basketInfo._data);
                if (results._isSuccess) return _operationResult.Success("Success", results._data);
            }


            return _operationResult.Failed("Failed");


            // _contextDb.BasketInfos.Add(receiveBasketDTO.basket);
        }
        catch (Exception e)
        {
            return _operationResult.Failed(e.Message);
        }
    }
}