namespace PaymentGateway.Services.Repositories.Redis;

public interface IRedisRepositories
{
    public Task<T> GetAsync<T>(string key);
    public Task SetAsync<T>(string key, T? value, TimeSpan? expiry = null);
    public Task<bool> DeleteAsync(string key);

    public Task<bool> ExistsAsync(string key);

    // Update
    public Task<bool> UpdateAsync<T>(string key, T value);
}