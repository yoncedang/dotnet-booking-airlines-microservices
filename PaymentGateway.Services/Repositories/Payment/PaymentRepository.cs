using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PaymentGateway.Services.ApacheKafka.Producer;
using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.DbContextPayment;
using PaymentGateway.Services.DTOs;
using PaymentGateway.Services.Repositories.Redis;
using PaymentGateway.Services.Repositories.Stripe;
using Stripe.Checkout;

namespace PaymentGateway.Services.Repositories.Payment;

public class PaymentRepository : IPaymentRepository
{
    private readonly PaymentContextDB _contextDb;
    private readonly IKafkaProducer _kafkaProducer;
    private readonly IMapper _mapper;
    private readonly OperationResult<Session> _operationResult;

    private readonly IRedisRepositories _redis;
    private readonly SessionService _sessionService;
    private readonly IStripeRepository _stripeRepository;

    public PaymentRepository(IRedisRepositories redis, OperationResult<Session> operationResult,
        PaymentContextDB paymentContextDb, IStripeRepository stripeRepository,
        IKafkaProducer kafkaProducer, SessionService sessionService, IMapper mapper)
    {
        _operationResult = operationResult;
        _redis = redis;
        _contextDb = paymentContextDb;
        _stripeRepository = stripeRepository;
        _kafkaProducer = kafkaProducer;
        _sessionService = sessionService;
        _mapper = mapper;
    }

    public async Task<OperationResult<Session>> GetPaymentAsync(Guid userId)
    {
        try
        {
            var checkUser = await _contextDb.BasketInfos
                .Where(x => x.UserId == userId && x.isPending == true && x.Status.ToLower() == "pending")
                .FirstOrDefaultAsync();

            if (checkUser == null) return _operationResult.Failed("Failed to get payment - Please add to Basket");

            var results = await _stripeRepository.GetPaymentAsync(checkUser);
            if (results._isSuccess)
            {
                await _kafkaProducer.SendMessageGeneric("HoldingTicket", Guid.NewGuid().ToString(),
                    checkUser);
                return _operationResult.Success("Success", results._data);
            }

            return _operationResult.Failed(results._message);
        }
        catch (Exception e)
        {
            Console.WriteLine("LOI NHA: " + e.Message);
            return _operationResult.Failed(e.Message);
        }
    }

    // public async Task<OperationResult<SessionDTO>> SuccessPaymentAsync(string sessionId)
    // {
    //     try
    //     {
    //         var session = await _sessionService.GetAsync(sessionId);
    //
    //         if (session.PaymentStatus.ToLower() == "unpaid")
    //             return new OperationResult<SessionDTO>().Failed("Payment is not paid yet");
    //
    //         var newdata = new SessionDTO
    //         {
    //             paymentStatus = session.PaymentStatus,
    //             metadata = new MetaData
    //             {
    //                 UserId = ParseGuid(session.Metadata["UserId"]),
    //                 BasketId = ParseGuid(session.Metadata["BasketId"]),
    //                 Price = ParseDouble(session.Metadata["Price"]),
    //                 AmountTicket = ParseInt(session.Metadata["AmountTicket"]),
    //                 FlightComeId = ParseGuid(session.Metadata["FlightComeId"]),
    //                 FlightBackId = ParseGuidNull(session.Metadata["FlightBackId"]),
    //                 IsPending = ParseBool(session.Metadata["IsPending"]),
    //                 Status = session.Metadata["Status"],
    //                 AmountPassenger = ParseInt(session.Metadata["AmountPassenger"])
    //             }
    //         };
    //
    //         var deleteBasketInfo = await _contextDb.BasketInfos
    //             .Where(x => x.BasketId == newdata.metadata.BasketId && x.UserId == newdata.metadata.UserId)
    //             .FirstOrDefaultAsync();
    //
    //         if (deleteBasketInfo != null)
    //         {
    //             _contextDb.BasketInfos.Remove(deleteBasketInfo);
    //             await _contextDb.SaveChangesAsync();
    //         }
    //
    //         await Task.WhenAll(
    //             Task.Run(() =>
    //                 _kafkaProducer.SendMessageGeneric("PaymentSuccess", Guid.NewGuid().ToString(), newdata)
    //             ),
    //             Task.Run(() =>
    //                 _kafkaProducer.SendMessageGeneric("BasketCreateCode", Guid.NewGuid().ToString(), newdata)
    //             )
    //         );
    //
    //         return new OperationResult<SessionDTO>().Success("Payment is successful", newdata);
    //     }
    //     catch (Exception e)
    //     {
    //         Console.WriteLine("LOI NHA 2: " + e.Message);
    //         return new OperationResult<SessionDTO>().Failed(e.Message);
    //     }
    // }

    public async Task<OperationResult<SessionDTO>> SuccessPaymentAsync(Session session)
    {
        try
        {
            if (session.PaymentStatus.ToLower() == "unpaid")
                return new OperationResult<SessionDTO>().Failed("Payment is not paid yet");

            var newdata = new SessionDTO
            {
                paymentStatus = session.PaymentStatus,
                metadata = new MetaData
                {
                    UserId = ParseGuid(session.Metadata["UserId"]),
                    BasketId = ParseGuid(session.Metadata["BasketId"]),
                    Price = ParseDouble(session.Metadata["Price"]),
                    AmountTicket = ParseInt(session.Metadata["AmountTicket"]),
                    FlightComeId = ParseGuid(session.Metadata["FlightComeId"]),
                    FlightBackId = ParseGuidNull(session.Metadata["FlightBackId"]),
                    IsPending = ParseBool(session.Metadata["IsPending"]),
                    Status = session.Metadata["Status"],
                    AmountPassenger = ParseInt(session.Metadata["AmountPassenger"])
                }
            };

            var deleteBasketInfo = await _contextDb.BasketInfos
                .Where(x => x.BasketId == newdata.metadata.BasketId && x.UserId == newdata.metadata.UserId)
                .FirstOrDefaultAsync();

            if (deleteBasketInfo != null)
            {
                _contextDb.BasketInfos.Remove(deleteBasketInfo);
                await _contextDb.SaveChangesAsync();
            }

            await Task.WhenAll(
                Task.Run(() =>
                    _kafkaProducer.SendMessageGeneric("PaymentSuccess", Guid.NewGuid().ToString(), newdata)
                ),
                Task.Run(() =>
                    _kafkaProducer.SendMessageGeneric("BasketCreateCode", Guid.NewGuid().ToString(), newdata)
                )
            );

            return new OperationResult<SessionDTO>().Success("Payment is successful", newdata);
        }
        catch (Exception e)
        {
            Console.WriteLine("LOI NHA 2: " + e.Message);
            return new OperationResult<SessionDTO>().Failed(e.Message);
        }
    }

    public async Task<OperationResult<SessionDTO>> UnpaidPaymentAsync(Session session)
    {
        try
        {
            if (session.PaymentStatus.ToLower() == "paid")
                return new OperationResult<SessionDTO>().Failed("Payment already paid");

            var newdata = new SessionDTO
            {
                paymentStatus = session.PaymentStatus,
                metadata = new MetaData
                {
                    UserId = ParseGuid(session.Metadata["UserId"]),
                    BasketId = ParseGuid(session.Metadata["BasketId"]),
                    Price = ParseDouble(session.Metadata["Price"]),
                    AmountTicket = ParseInt(session.Metadata["AmountTicket"]),
                    FlightComeId = ParseGuid(session.Metadata["FlightComeId"]),
                    FlightBackId = ParseGuidNull(session.Metadata["FlightBackId"]),
                    IsPending = ParseBool(session.Metadata["IsPending"]),
                    Status = session.Metadata["Status"],
                    AmountPassenger = ParseInt(session.Metadata["AmountPassenger"])
                }
            };

            var deleteBasketInfo = await _contextDb.BasketInfos
                .Where(x => x.BasketId == newdata.metadata.BasketId && x.UserId == newdata.metadata.UserId)
                .FirstOrDefaultAsync();

            if (deleteBasketInfo != null)
            {
                _contextDb.BasketInfos.Remove(deleteBasketInfo);
                await _contextDb.SaveChangesAsync();
            }

            await Task.WhenAll(
                Task.Run(() =>
                    _kafkaProducer.SendMessageGeneric("PaymentCancel", Guid.NewGuid().ToString(), newdata)
                ),
                Task.Run(() =>
                    _kafkaProducer.SendMessageGeneric("PaymentCancelBasket", Guid.NewGuid().ToString(), newdata)
                )
            );

            return new OperationResult<SessionDTO>().Success("Payment cancel success", newdata);
        }
        catch (Exception e)
        {
            Console.WriteLine("LOI NHA 2: " + e.Message);
            return new OperationResult<SessionDTO>().Failed(e.Message);
        }
    }


    // Phương thức chuyển đổi chuỗi thành Guid một cách an toàn
    private Guid ParseGuid(string input)
    {
        Guid result;
        if (Guid.TryParse(input, out result))
            return result;
        throw new FormatException("Invalid Guid format");
    }


    private Guid? ParseGuidNull(string input)
    {
        if (Guid.TryParse(input, out var result))
            return result;
        return null;
    }

// Phương thức chuyển đổi chuỗi thành số nguyên một cách an toàn
    private int ParseInt(string input)
    {
        int result;
        if (int.TryParse(input, out result))
            return result;
        throw new FormatException("Invalid integer format");
    }

// Phương thức chuyển đổi chuỗi thành số thực một cách an toàn
    private double ParseDouble(string input)
    {
        double result;
        if (double.TryParse(input, out result))
            return result;
        throw new FormatException("Invalid double format");
    }

// Phương thức chuyển đổi chuỗi thành giá trị boolean một cách an toàn
    private bool ParseBool(string input)
    {
        bool result;
        if (bool.TryParse(input, out result))
            return result;
        throw new FormatException("Invalid boolean format");
    }
}