using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.DTOs;
using Stripe.Checkout;

namespace PaymentGateway.Services.Repositories.Payment;

public interface IPaymentRepository
{
    // PaymentWith UserId
    Task<OperationResult<Session>> GetPaymentAsync(Guid userId);

    // success
    Task<OperationResult<SessionDTO>> SuccessPaymentAsync(Session session);

    Task<OperationResult<SessionDTO>> UnpaidPaymentAsync(Session session);
}