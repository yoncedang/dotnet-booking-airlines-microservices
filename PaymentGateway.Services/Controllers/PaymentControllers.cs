using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PaymentGateway.Services.DTOs;
using PaymentGateway.Services.Repositories.Payment;
using Stripe.Checkout;

namespace PaymentGateway.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class paymentController : ControllerBase
{
    private readonly string _API_KEY;
    private readonly IConfiguration _configuration;
    private readonly HttpClient _httpClient;
    private readonly PaymentDTOs _paymentDTOs;
    private readonly IPaymentRepository _paymentRepository;
    private readonly SessionService _sessionService;

    public paymentController(IPaymentRepository paymentRepository, PaymentDTOs paymentDTOs,
        SessionService sessionService, IHttpClientFactory httpClientFactory, IConfiguration configuration)
    {
        _paymentRepository = paymentRepository;
        _paymentDTOs = paymentDTOs;
        _sessionService = sessionService;
        _httpClient = httpClientFactory.CreateClient();
        _configuration = configuration;
        _API_KEY = _configuration["StripeConfiguration:SecretKey"];
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
    [HttpGet("checkout/user")]
    public async Task<IActionResult> GetPaymentAsync()
    {
        try
        {
            var userId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var payment = await _paymentRepository.GetPaymentAsync(userId);
            if (!payment._isSuccess)
                return BadRequest(new
                {
                    message = payment._message
                });

            _paymentDTOs.StripeURL = payment._data.Url;
            HttpContext.Response.Cookies.Append("SessionId", payment._data.Id, new CookieOptions
            {
                Expires = DateTimeOffset.Now + TimeSpan.FromMinutes(30),
                HttpOnly = true,
                // Secure = false,
                SameSite = SameSiteMode.Strict,
                Path = "/"
            });
            return Ok(new
            {
                message = "We are redirecting you to the Stripe payment page.",
                holdingTicket = true,
                holdingDuration = "Holding tikcet duration in 30 minutes",
                stripeURL = _paymentDTOs.StripeURL,
                cardTesting = new
                {
                    Number = "4242424242424242",
                    ExpMonth = "Any month <= 12",
                    ExpYear = "This year or next year",
                    Cvc = "Any 3 digits"
                },
                sessionId = payment._data.Id
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [AllowAnonymous]
    [HttpGet("checkout/anonymous/{Id}")]
    public async Task<IActionResult> GetPaymentAnonymounsIdAsync(Guid Id)
    {
        try
        {
            var payment = await _paymentRepository.GetPaymentAsync(Id);
            if (!payment._isSuccess)
                return BadRequest(new
                {
                    message = payment._message
                });
            HttpContext.Response.Cookies.Append("SessionId", payment._data.Id, new CookieOptions
            {
                Expires = DateTimeOffset.Now + TimeSpan.FromMinutes(30),
                HttpOnly = true,
                // Secure = true,
                SameSite = SameSiteMode.Strict,
                Path = "/"
            });
            _paymentDTOs.StripeURL = payment._data.Url;


            return Ok(new
            {
                message = "We are redirecting you to the Stripe payment page.",
                holdingTicket = true,
                holdingDuration = new DateTimeOffset() + TimeSpan.FromMinutes(30),
                stripeURL = _paymentDTOs.StripeURL,
                cardTesting = new
                {
                    Number = "4242424242424242",
                    ExpMonth = "Any month <= 12",
                    ExpYear = "This year or next year",
                    Cvc = "Any 3 digits"
                },
                sessionId = payment._data.Id
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [AllowAnonymous]
    [HttpGet("success")]
    public async Task<IActionResult> SuccessAsync()
    {
        try
        {
            var sessionIdCookie = HttpContext.Request.Cookies["SessionId"];

            if (sessionIdCookie == null)
                return BadRequest(new
                {
                    message = "SessionId not found"
                });
            var client = new HttpClient();
            // var API_KEY = _configuration["StripeConfiguration:SecretKey"];
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {_API_KEY}");
            var response = await client.GetAsync($"https://api.stripe.com/v1/checkout/sessions/{sessionIdCookie}");
            var responseContent = await response.Content.ReadAsStringAsync();
            var session = JsonConvert.DeserializeObject<Session>(responseContent);
            // var responseContent = await response.Content.ReadAsStringAsync();

            var payment = await _paymentRepository.SuccessPaymentAsync(session);

            if (!payment._isSuccess)
                return BadRequest(new
                {
                    message = payment._message
                });

            HttpContext.Response.Cookies.Delete("SessionId");
            return Ok(new
            {
                message = payment._message,
                status = payment._data.paymentStatus
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [AllowAnonymous]
    [HttpGet("cancel")]
    public async Task<IActionResult> CancelAsync()
    {
        try
        {
            var sessionIdCookie = HttpContext.Request.Cookies["SessionId"];
            if (sessionIdCookie == null)
                return BadRequest(new
                {
                    message = "SessionId not found"
                });

            var client = new HttpClient();
            // var API_KEY = _configuration["StripeConfiguration:SecretKey"];
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {_API_KEY}");
            var response = await client.GetAsync($"https://api.stripe.com/v1/checkout/sessions/{sessionIdCookie}");
            var responseContent = await response.Content.ReadAsStringAsync();
            var session = JsonConvert.DeserializeObject<Session>(responseContent);

            var payment = await _paymentRepository.UnpaidPaymentAsync(session);
            if (!payment._isSuccess)
                return BadRequest(new
                {
                    message = payment._message
                });

            HttpContext.Response.Cookies.Delete("SessionId");
            return Ok(new
            {
                message = payment._message
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }
}