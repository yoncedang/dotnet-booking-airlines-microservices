namespace PaymentGateway.Services.Assistant;

public class OperationResult<T>
{
    public bool _isSuccess { get; set; }
    public string _message { get; set; }

    public T _data { get; set; }

    // success result
    public OperationResult<T> Success(string message, T data)
    {
        return new OperationResult<T>
        {
            _isSuccess = true,
            _message = message,
            _data = data
        };
    }


    public OperationResult<T> Failed(string message)
    {
        return new OperationResult<T>
        {
            _isSuccess = false,
            _message = message
        };
    }
}