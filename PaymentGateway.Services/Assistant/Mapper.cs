using AutoMapper;
using PaymentGateway.Services.ApacheKafka.DataReceiveDTO;
using PaymentGateway.Services.DTOs;
using PaymentGateway.Services.Models;

namespace PaymentGateway.Services.Assistant;

public class ProfileMapper : Profile
{
    public ProfileMapper()
    {
        CreateMap<object, ReceiveBasketDTO>().ReverseMap();
        CreateMap<object, BasketDTO>().ReverseMap();

        CreateMap<BasketInfo, BasketDTOs>().ReverseMap();
    }
}