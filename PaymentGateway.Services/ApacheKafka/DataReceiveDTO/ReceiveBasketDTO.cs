namespace PaymentGateway.Services.ApacheKafka.DataReceiveDTO;

public class ReceiveBasketDTO
{
    public string role { get; set; }
    public BasketDTO basket { get; set; }
}