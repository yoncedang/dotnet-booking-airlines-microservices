using AutoMapper;
using Confluent.Kafka;
using PaymentGateway.Services.ApacheKafka.DataReceiveDTO;
using PaymentGateway.Services.Repositories.Consumer;
using PaymentGateway.Services.Repositories.Redis;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace PaymentGateway.Services.ApacheKafka.Consumer;

public class KafkaConsumer : IHostedService, IKafkaConsumer
{
    private readonly IConfiguration _configuration;
    private readonly IConsumer<string, string> _consumer;
    private readonly IConsumerRepository _consumerRepository;
    private readonly IMapper _mapper;
    private readonly IRedisRepositories _redis;


    public KafkaConsumer(IRedisRepositories redis, IConsumerRepository consumerRepository, IMapper mapper,
        IConfiguration configuration)
    {
        _configuration = configuration;
        var _config = new ConsumerConfig
        {
            BootstrapServers = _configuration["Kafka:BootstrapServers"],
            GroupId = "Payments.Services",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnableAutoCommit = false
        };

        _redis = redis;
        _consumerRepository = consumerRepository;
        _mapper = mapper;
        _consumer = new ConsumerBuilder<string, string>(_config).Build();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var listTOPIC = new List<ConsumerTOPIC>
        {
            new() { topic = "BasketAddingTopic" }
        };

        foreach (var topic in listTOPIC)
        {
            Console.WriteLine($"Subscribing to topic: {topic.topic}");
            _consumer.Subscribe(topic.topic);
        }

        await ExecuteAsync(cancellationToken);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _consumer.Close();
    }

    public async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    Console.WriteLine("Kafka Consumer is listening...");
                    var consumeResult = _consumer.Consume(stoppingToken);
                    var key = consumeResult.Message.Key;
                    var topic = consumeResult.Topic;
                    var value = consumeResult.Message.Value;

                    switch (topic)
                    {
                        case "BasketAddingTopic":
                            var checkKey = await _redis.ExistsAsync(consumeResult.Message.Key);
                            if (!checkKey)
                            {
                                Console.WriteLine("Key not found");
                                break;
                            }

                            Console.WriteLine("Key: " + consumeResult.Message.Key);
                            var receiveBasketDTO =
                                JsonSerializer.Deserialize<ReceiveBasketDTO>(value);

                            Console.WriteLine("FlightComeId: " + receiveBasketDTO.basket.FlightComeId);

                            var results = await _consumerRepository.CheckUserId(receiveBasketDTO);

                            if (results._isSuccess)
                            {
                                await _redis.DeleteAsync(consumeResult.Message.Key);
                                Console.WriteLine("DeletedKey: " + consumeResult.Message.Key);
                                _consumer.Commit();
                                Console.WriteLine("Commited");
                            }

                            break;
                        default:
                            Console.WriteLine($"Unhandled key: {topic}");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        catch (OperationCanceledException e)
        {
            await StopAsync(stoppingToken);
        }
        finally
        {
            _consumer.Close();
        }
    }
}