namespace PaymentGateway.Services.ApacheKafka.Consumer;

public interface IKafkaConsumer
{
    Task StartAsync(CancellationToken cancellationToken);
    Task StopAsync(CancellationToken cancellationToken);
    Task ExecuteAsync(CancellationToken stoppingToken);
}