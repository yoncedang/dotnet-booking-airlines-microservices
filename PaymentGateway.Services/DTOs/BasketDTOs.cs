namespace PaymentGateway.Services.DTOs;

public class BasketDTOs
{
    public Guid Id { get; set; }

    public Guid BasketId { get; set; }

    public Guid UserId { get; set; }

    public Guid FlightComeId { get; set; }

    public Guid? FlightBackId { get; set; }

    public int AmountPassenger { get; set; }

    public int AmountTicket { get; set; }

    public double Price { get; set; }

    public string AirlinesCome { get; set; }

    public string? AirlinesBack { get; set; }

    public DateTimeOffset CreateAt { get; set; }

    public DateTimeOffset UpdateAt { get; set; }

    public bool isPending { get; set; }

    public string Status { get; set; }
}