using System.ComponentModel.DataAnnotations;

namespace PaymentGateway.Services.Models;

public class BasketInfo
{
    [Key] public Guid Id { get; set; }

    [Required(ErrorMessage = "BasketId is required")]
    public Guid BasketId { get; set; }

    [Required(ErrorMessage = "UserId is required")]
    public Guid UserId { get; set; }

    [Required(ErrorMessage = "AmountPassenger is required")]
    public int AmountPassenger { get; set; }

    [Required(ErrorMessage = "AmountTicket is required")]
    public int AmountTicket { get; set; }

    [Required(ErrorMessage = "Price is required")]
    public double Price { get; set; }

    [Required(ErrorMessage = "AirlinesCome is required")]
    public string AirlinesCome { get; set; }

    [Required(ErrorMessage = "FlightComeId is required")]
    public Guid FlightComeId { get; set; }

    public Guid? FlightBackId { get; set; }
    public string? AirlinesBack { get; set; }

    public bool isPending { get; set; }

    public string Status { get; set; }

    public DateTimeOffset CreateAt { get; set; }

    public DateTimeOffset UpdateAt { get; set; }
}