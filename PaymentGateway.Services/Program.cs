using PaymentGateway.Services.ApacheKafka.Consumer;
using PaymentGateway.Services.Startup;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
// builder.WebHost.HttpsBackendmicro();

// new KafkaConsumer();
new Startup(builder.Services, builder.Configuration);
var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseCors("DevelopmentPolicy");
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseCors("ProductionPolicy");
    app.UseHttpsRedirection();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

// app.UseHangfireServer();
// app.UseHangfireDashboard();

app.MapControllers();
app.MapGet("/", context =>
{
    // redirect to swagger
    context.Response.Redirect("/swagger/index.html");

    return Task.CompletedTask;
});

var host = app.Services.GetRequiredService<IHostApplicationLifetime>();

host.ApplicationStarted.Register(async () =>
{
    await Task.Delay(1);

    using (var scope = app.Services.CreateScope())
    {
        var serviceProvider = scope.ServiceProvider;

        // Start IkafkaConsumer
        var kafkaConsumer = serviceProvider.GetRequiredService<IKafkaConsumer>();
        await kafkaConsumer.StartAsync(CancellationToken.None);
    }
});

app.Run();

// Tạo background task để khởi chạy Kafka Consumer sau khi ứng dụng đã bắt đầu chạy