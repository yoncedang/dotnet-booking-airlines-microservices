using Microsoft.EntityFrameworkCore;
using PaymentGateway.Services.Models;

namespace PaymentGateway.Services.DbContextPayment;

public class PaymentContextDB : DbContext
{
    public PaymentContextDB(DbContextOptions<PaymentContextDB> options) : base(options)
    {
    }

    public DbSet<BasketInfo> BasketInfos { get; set; }

    // protected override void OnModelCreating(ModelBuilder modelBuilder)
    // {
    //     base.OnModelCreating(modelBuilder);
    // }
}