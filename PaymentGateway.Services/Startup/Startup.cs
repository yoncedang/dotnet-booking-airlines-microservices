using System.Text;
using System.Text.Json.Serialization;
using Confluent.Kafka;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PaymentGateway.Services.ApacheKafka.Consumer;
using PaymentGateway.Services.ApacheKafka.Producer;
using PaymentGateway.Services.Assistant;
using PaymentGateway.Services.DbContextPayment;
using PaymentGateway.Services.DTOs;
using PaymentGateway.Services.Repositories.Consumer;
using PaymentGateway.Services.Repositories.Payment;
using PaymentGateway.Services.Repositories.Redis;
using PaymentGateway.Services.Repositories.Stripe;
using StackExchange.Redis;
using Stripe.Checkout;

namespace PaymentGateway.Services.Startup;

public class Startup
{
    private readonly IConfiguration _configuration;
    private readonly IServiceCollection _service;

    public Startup(IServiceCollection service, IConfiguration configuration)
    {
        _service = service;
        _configuration = configuration;


        AutoMapper();
        JsonAccepted();
        AddHelpers();
        OperationResults();
        Redis();
        connectDb();
        Migration();
        Repositories();
        Swagger();
        JwtBearer();
        AddHttpsRedirection();
        Cors();
        // Hangfire();
        AddServices();
        KafkaRegisterProducer();
    }


    private void Swagger()
    {
        _service.AddSwaggerGen(option =>
        {
            option.SwaggerDoc("v1", new OpenApiInfo { Title = "Payment API", Version = "v1" });
            option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please enter a valid token",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "Bearer"
            });
            option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
        });
    }

    private void JwtBearer()
    {
        _service.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _configuration["JWT:ValidIssuer"],
                ValidateAudience = true,
                ValidAudience = _configuration["JWT:ValidAudience"],

                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:IssuerSigningKey"])),
                ValidateIssuerSigningKey = true,

                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true,
                RequireExpirationTime = true
            };
        });
    }

    private void connectDb()
    {
        // Postgres
        _service.AddDbContext<PaymentContextDB>(options =>
        {
            options.UseNpgsql(_configuration.GetConnectionString("PaymentDB"));
        });
        if (_service.BuildServiceProvider().GetService<PaymentContextDB>() is not null)
            Console.WriteLine("PaymentDB connected");
    }

    private void Migration()
    {
        using var scope = _service.BuildServiceProvider().CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<PaymentContextDB>();
        context.Database.Migrate();
    }

    private void KafkaRegisterProducer()
    {
        _service.AddSingleton<ProducerConfig>(p =>
        {
            return new ProducerConfig
            {
                BootstrapServers = _configuration["Kafka:BootstrapServers"],
                ClientId = "Identity.Services",
                AllowAutoCreateTopics = true,
                EnableIdempotence = true
            };
        });

        // _service.AddSingleton<typeof()>();
    }


    private void Hangfire()
    {
        _service.AddHangfire(config =>
        {
            config.UsePostgreSqlStorage(_configuration.GetConnectionString("PaymentDB"));
        });

        _service.AddHangfireServer();


        if (_service.BuildServiceProvider().GetService<IBackgroundJobClient>() is not null)
            Console.WriteLine("Hangfire connected");
    }

    private void Cors()
    {
        _service.AddCors(options =>
        {
            options.AddPolicy("DevelopmentPolicy", builder =>
            {
                builder.WithOrigins("http://localhost:8200")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });
            options.AddPolicy("ProductionPolicy", builder =>
            {
                builder.WithOrigins("https://localhost:8201")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });
        });
    }

    private void AddHttpsRedirection()
    {
        _service.AddHttpsRedirection(options =>
        {
            options.HttpsPort = 443;
            options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
        });
    }

    private void AutoMapper()
    {
        _service.AddAutoMapper(typeof(Startup));
    }

    private void JsonAccepted()
    {
        _service.AddControllers().AddJsonOptions(x =>
            x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
    }

    private void AddHelpers()
    {
        _service.AddScoped<ConsumerRepositoryHelper>();
    }

    private void OperationResults()
    {
        _service.AddScoped(typeof(OperationResult<>));
    }

    private void Redis()
    {
        var redis = ConnectionMultiplexer.Connect(_configuration["Redis:Connection"]);
        _service.AddSingleton<IConnectionMultiplexer>(redis);

        if (redis.IsConnected)
        {
            Console.WriteLine("Redis is connected");
        }
        else
        {
            Console.WriteLine("Redis is not connected");
            throw new Exception("Redis is not connected");
        }
    }

    private void AddServices()
    {
        // _service.AddHttpContextAccessor();
        _service.AddMemoryCache();
        _service.AddScoped<PaymentDTOs>();
        _service.AddScoped<SessionService>();
        _service.AddHttpClient();
        // _service.AddSession();
        // httpContext
        // _service.AddScoped<IHttpContextAccessor>();
    }

    private void Repositories()
    {
        _service.AddScoped<IRedisRepositories, RedisRepositories>();
        _service.AddScoped<IKafkaConsumer, KafkaConsumer>();
        _service.AddScoped<IConsumerRepository, ConsumerRepository>();
        _service.AddScoped<IPaymentRepository, PaymentRepository>();
        _service.AddScoped<IStripeRepository, StripeRepository>();
        _service.AddScoped<IKafkaProducer, KafkaProducer>();
    }
}