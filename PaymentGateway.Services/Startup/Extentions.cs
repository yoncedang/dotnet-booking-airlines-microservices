using Newtonsoft.Json;

namespace PaymentGateway.Services.Startup;

public class Extentions
{
    private readonly RequestDelegate _next;

    public Extentions(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        // Kiểm tra xem header Authorization có tồn tại và có chứa JWT không
        if (!context.Request.Headers.ContainsKey("Authorization"))
        {
            await HandleUnauthenticatedAsync(context);
            return;
        }

        var authHeader = context.Request.Headers["Authorization"].ToString();
        if (!authHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
        {
            await HandleUnauthenticatedAsync(context);
            return;
        }

        var token = authHeader.Substring("Bearer ".Length).Trim();

        // Kiểm tra tính hợp lệ của token ở đây
        // Nếu token không hợp lệ, gọi HandleUnauthenticatedAsync

        await _next(context);
    }

    private async Task HandleUnauthenticatedAsync(HttpContext context)
    {
        context.Response.StatusCode = 401;
        context.Response.ContentType = "application/json";

        var errorResponse = new
        {
            error = "Unauthorized",
            message = "JWT not provided."
        };


        await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponse));
    }
}

public static class TokenVerificationMiddlewareExtensions
{
    public static IApplicationBuilder UseTokenVerification(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<Extentions>();
    }
}