using Flight.Services.Models;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.ContextDB;

public class ApplicationDBContext : DbContext
{
    public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
    {
    }

    public DbSet<Airports> Airports { get; set; }
    public DbSet<Airlines> Airlines { get; set; }
    public DbSet<Flights> Flights { get; set; }

    public DbSet<Aircraft> Aircrafts { get; set; }

    public DbSet<Hangfires> Hangfires { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        SeedAirlines(modelBuilder);
        SeedAirports(modelBuilder);
        FlightEntity(modelBuilder);
        SeedAircrafts(modelBuilder);
    }

    private void SeedAirlines(ModelBuilder builder)
    {
        builder.Entity<Airlines>().HasData(
            new Airlines
            {
                Id = Guid.NewGuid(),
                Name = "Vietnam Airlines",
                Country = "Vietnam",
                IATA = "VNA"
            },
            new Airlines
            {
                Id = Guid.NewGuid(),
                Name = "Bamboo Airways",
                Country = "Vietnam",
                IATA = "BBA"
            }
        );
    }

    private void SeedAirports(ModelBuilder builder)
    {
        builder.Entity<Airports>().HasData(
            // Hà Nội
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Nội Bài International Airport",
                City = "Hà Nội",
                Country = "Việt Nam",
                IATA = "HAN"
            },
            // HCM
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Tân Sơn Nhất International Airport",
                City = "Hồ Chí Minh",
                Country = "Việt Nam",
                IATA = "SGN"
            },
            // Đà Nẵng
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Đà Nẵng International Airport",
                City = "Đà Nẵng",
                Country = "Việt Nam",
                IATA = "DAD"
            },
            // Điện Biên Phủ
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Điện Biên Phủ Airport",
                City = "Điện Biên",
                Country = "Việt Nam",
                IATA = "DIN"
            },
            // Hải Phòng
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Cát Bi International Airport",
                City = "Hải Phòng",
                Country = "Việt Nam",
                IATA = "HPH"
            },
            // Thanh Hóa
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Thọ Xuân Airport",
                City = "Thanh Hóa",
                Country = "Việt Nam",
                IATA = "THD"
            },
            // Vinh
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Vinh Airport",
                City = "Vinh",
                Country = "Việt Nam",
                IATA = "VII"
            },
            // Quảng Bình
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Đồng Hới Airport",
                City = "Quảng Bình",
                Country = "Việt Nam",
                IATA = "VDH"
            },
            // Quảng Nam
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Chu Lai International Airport",
                City = "Quảng Nam",
                Country = "Việt Nam",
                IATA = "VCL"
            },
            // Huế
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Phú Bài International Airport",
                City = "Huế",
                Country = "Việt Nam",
                IATA = "HUI"
            },
            // Pleiku
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Pleiku Airport",
                City = "Pleiku",
                Country = "Việt Nam",
                IATA = "PXU"
            },
            // Phú Yên
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Tuy Hòa Airport",
                City = "Phú Yên",
                Country = "Việt Nam",
                IATA = "TBB"
            },
            // Ban mê Thuột
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Buôn Ma Thuột Airport",
                City = "Ban Mê Thuột",
                Country = "Việt Nam",
                IATA = "BMV"
            },
            // Nha Trang
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Cam Ranh International Airport",
                City = "Nha Trang",
                Country = "Việt Nam",
                IATA = "CXR"
            },
            // Qui Nhơn
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Phù Cát Airport",
                City = "Qui Nhơn",
                Country = "Việt Nam",
                IATA = "UIH"
            },
            // Đà Lạt
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Liên Khương Airport",
                City = "Đà Lạt",
                Country = "Việt Nam",
                IATA = "DLI"
            },
            // Cần Thơ
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Cần Thơ International Airport",
                City = "Cần Thơ",
                Country = "Việt Nam",
                IATA = "VCA"
            },
            // Cà Mau
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Cà Mau Airport",
                City = "Cà Mau",
                Country = "Việt Nam",
                IATA = "CAH"
            },
            // Phú Quốc
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Phú Quốc International Airport",
                City = "Phú Quốc",
                Country = "Việt Nam",
                IATA = "PQC"
            },
            // Côn Đảo
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Côn Đảo Airport",
                City = "Côn Đảo",
                Country = "Việt Nam",
                IATA = "VCS"
            },
            // Vân Đồn
            new Airports
            {
                Id = Guid.NewGuid(),
                Name = "Vân Đồn International Airport",
                City = "Vân Đồn",
                Country = "Việt Nam",
                IATA = "VDO"
            }
        );
    }

    private void SeedAircrafts(ModelBuilder builder)
    {
        builder.Entity<Aircraft>().HasData(
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Airbus",
                Model = "A320",
                TotalSeats = 190,
                Active = true
            },
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Airbus",
                Model = "A321",
                TotalSeats = 220,
                Active = true
            },
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Boeing",
                Model = "B787",
                TotalSeats = 250,
                Active = true
            },
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Boeing",
                Model = "B737",
                TotalSeats = 250,
                Active = true
            },
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Airbus",
                Model = "A350",
                TotalSeats = 300,
                Active = true
            },
            new Aircraft
            {
                Id = Guid.NewGuid(),
                Name = "Boeing",
                Model = "B777",
                TotalSeats = 300,
                Active = true
            }
        );
    }

    private void FlightEntity(ModelBuilder builder)
    {
        builder.Entity<Flights>().HasOne<Airlines>(f => f.Airline).WithMany(s => s.Flights)
            .HasForeignKey(f => f.AirlineId);
        builder.Entity<Flights>().HasOne<Airports>(f => f.DepartureAirport).WithMany(s => s.DepartureFlights)
            .HasForeignKey(f => f.ArrivalAirportId);

        builder.Entity<Flights>().HasOne<Airports>(f => f.ArrivalAirport).WithMany(s => s.ArrivalFlights)
            .HasForeignKey(f => f.DepartureAirportId);
        builder.Entity<Flights>().HasOne<Aircraft>(f => f.Aircraft).WithMany(s => s.Flights)
            .HasForeignKey(f => f.AircraftId);
    }
}