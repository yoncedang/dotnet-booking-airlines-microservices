using Flight.Services.Startup;
using Hangfire;
using Payment.Services.ApacheKafka;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
// builder.WebHost.HttpsBackendmicro();

new Startup(builder.Services, builder.Configuration);

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors("Development");
}
else
{
    app.UseCors("Production");
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseHttpsRedirection();
}

app.UseAuthentication();
app.UseAuthorization();
app.UseHangfireServer();
app.UseHangfireDashboard();

app.MapControllers();
app.MapGet("/", context =>
{
    // redirect to swagger
    context.Response.Redirect("/swagger/index.html");

    return Task.CompletedTask;
});
var host = app.Services.GetRequiredService<IHostApplicationLifetime>();

host.ApplicationStarted.Register(async () =>
{
    await Task.Delay(1);

    // Start Kafka Consumer
    // var kafkaConsumer = new KafkaConsumer(app.Services.GetRequiredService<IRedisRepositories>());
    // kafkaConsumer.StartAsync(CancellationToken.None);
    using (var scope = app.Services.CreateScope())
    {
        var serviceProvider = scope.ServiceProvider;

        // Start IkafkaConsumer
        var kafkaConsumer = serviceProvider.GetRequiredService<IKafkaConsumer>();
        await kafkaConsumer.StartAsync(CancellationToken.None);
    }
});
app.Run();