using System.Text;
using System.Text.Json.Serialization;
using Flight.Services.ApacheKafka.Consumer;
using Flight.Services.ApacheKafka.Producer;
using Flight.Services.Assistant;
using Flight.Services.Assistant.LocgicHelp;
using Flight.Services.ContextDB;
using Flight.Services.Repositories.Aircraft;
using Flight.Services.Repositories.Airport;
using Flight.Services.Repositories.Consumer;
using Flight.Services.Repositories.Flight;
using Flight.Services.Repositories.Redis;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Payment.Services.ApacheKafka;
using StackExchange.Redis;

namespace Flight.Services.Startup;

public class Startup
{
    private readonly IConfiguration _configuration;
    private readonly IServiceCollection _service;

    public Startup(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        _configuration = configuration;
        _service = serviceCollection;
        ConnectDB();
        Migration();
        Cors();
        AddHttpsRedirection();
        JwtBearer();
        Swagger();
        Mapper();
        Repository();
        AddServices();
        Redis();
        OperationResults();
        Hangfire();
        HangFireJobs();
    }

    private void ConnectDB()
    {
        // Postgres
        _service.AddDbContext<ApplicationDBContext>(options =>
        {
            options.UseNpgsql(_configuration.GetConnectionString("FlightDB"));
        });
        if (_service.BuildServiceProvider().GetService<ApplicationDBContext>() is not null)
            Console.WriteLine("FlightDB connected");
    }

    private void Migration()
    {
        using var scope = _service.BuildServiceProvider().CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<ApplicationDBContext>();
        context.Database.Migrate();
    }

    private void Hangfire()
    {
        _service.AddHangfire(config =>
        {
            config.UsePostgreSqlStorage(_configuration.GetConnectionString("FlightDB"));
        });

        _service.AddHangfireServer();


        if (_service.BuildServiceProvider().GetService<IBackgroundJobClient>() is not null)
            Console.WriteLine("Hangfire connected");
    }

    private void HangFireJobs()
    {
        RecurringJob.AddOrUpdate<IFlightRepositories>("Auto Update Flight Status", repo => repo.AutoUpdateStatus(),
            Cron.Minutely);

        RecurringJob.AddOrUpdate<IFlightRepositories>("Hangfires Expired", repo => repo.HangfiresExpired(),
            Cron.Minutely);
    }

    private void Cors()
    {
        _service.AddCors(options =>
        {
            // Development
            options.AddPolicy("Development", builder =>
            {
                builder
                    .WithOrigins("http://localhost:1080")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });

            // Production
            options.AddPolicy("Production", builder =>
            {
                builder
                    .WithOrigins("https://localhost:1080")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });
        });
    }

    private void AddHttpsRedirection()
    {
        _service.AddHttpsRedirection(options =>
        {
            options.HttpsPort = 443;
            options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
        });
    }


    private void JwtBearer()
    {
        _service.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _configuration["JWT:ValidIssuer"],
                ValidateAudience = true,
                ValidAudience = _configuration["JWT:ValidAudience"],

                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:IssuerSigningKey"])),
                ValidateIssuerSigningKey = true,

                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true,
                RequireExpirationTime = true
            };
        });
    }

    private void Swagger()
    {
        _service.AddSwaggerGen(option =>
        {
            option.SwaggerDoc("v1", new OpenApiInfo { Title = "Flight.Services API", Version = "v1" });
            option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please enter a valid token",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "Bearer"
            });
            option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
        });
    }

    private void Mapper()
    {
        _service.AddAutoMapper(typeof(ProfileMapper));
    }

    private void Repository()
    {
        // _service.AddScoped<IFlightRepositories, FlightRepositories>();
        _service.AddScoped<IRedisRepositories, RedisRepositories>();
        _service.AddScoped<IAirportRepositories, AirportRepositories>();
        _service.AddScoped<IFlightRepositories, FlightRepositories>();
        _service.AddScoped<IAircraftRepositories, AircraftRepositories>();
        _service.AddScoped<IKafkaConsumer, KafkaConsumer>();
        _service.AddScoped<IConsumerRepository, ConsumerRepository>();
        _service.AddScoped<IKafkaGroupdId, KafkaGroupId>();
        _service.AddScoped<IKafkaProducer, KafkaProducer>();
    }


    private void AddServices()
    {
        // add AirportHelper
        _service.AddScoped<AirportHelper>();
        _service.AddScoped<FlightHelper>();


        // add FlightHelper
        // _service.AddScoped<FlightHelper>();
        _service.AddControllers().AddJsonOptions(x =>
            x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
    }

    private void OperationResults()
    {
        _service.AddScoped(typeof(OperationResult<>));
    }

    private void Redis()
    {
        var Redis = ConnectionMultiplexer.Connect(_configuration["Redis:Connection"]);
        _service.AddSingleton<IConnectionMultiplexer>(Redis);

        Console.WriteLine($"Redis is connected: {Redis.IsConnected}");
    }
}