namespace Flight.Services.Startup;

public static class HttpsConfiguration
{
    public static void HttpsBackendmicro(this ConfigureWebHostBuilder hostBuilder)
    {
        hostBuilder.UseKestrel(options =>
        {
            var certificatePath = Path.Combine("certificates", "backendmicro.pfx");
            options.ListenAnyIP(443,
                listenOptions => { listenOptions.UseHttps(certificatePath, "181199"); });

            options.ListenAnyIP(80);
        });
    }
}