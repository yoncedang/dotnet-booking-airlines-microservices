using System.ComponentModel.DataAnnotations;

namespace Flight.Services.Models;

public class Aircraft
{
    [Key] public Guid Id { get; set; }

    public string Name { get; set; }
    public string Model { get; set; }
    public int TotalSeats { get; set; }
    public bool Active { get; set; }


    public ICollection<Flights> Flights { get; set; } = new List<Flights>();
}