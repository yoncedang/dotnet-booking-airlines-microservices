using System.ComponentModel.DataAnnotations;

namespace Flight.Services.Models;

public class Airports
{
    [Key] public Guid Id { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string IATA { get; set; }

    public ICollection<Flights> DepartureFlights { get; set; }
    public ICollection<Flights> ArrivalFlights { get; set; }
}