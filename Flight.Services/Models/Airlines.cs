using System.ComponentModel.DataAnnotations;

namespace Flight.Services.Models;

public class Airlines
{
    [Key] public Guid Id { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string IATA { get; set; }

    public ICollection<Flights> Flights { get; set; } = new List<Flights>();
}