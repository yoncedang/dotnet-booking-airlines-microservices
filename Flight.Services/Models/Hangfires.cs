using System.ComponentModel.DataAnnotations;

namespace Flight.Services.Models;

public class Hangfires
{
    [Key] public Guid Id { get; set; }

    public Guid FlightId { get; set; }

    public Guid BasketId { get; set; }

    public int AmountPassenger { get; set; }

    public DateTime Expired { get; set; } = DateTime.UtcNow.AddMinutes(30);
}