using System.ComponentModel.DataAnnotations;

namespace Flight.Services.Models;

public class Flights
{
    [Key] public Guid Id { get; set; }
    public string FlightCode { get; set; }
    public Guid DepartureAirportId { get; set; }

    public Airports DepartureAirport { get; set; }
    public Guid ArrivalAirportId { get; set; }

    public Airports ArrivalAirport { get; set; }

    // public DateTime Date { get; set; }
    public DateTimeOffset DepartureTime { get; set; }
    public DateTimeOffset ArrivalTime { get; set; }
    public int AvailableSeats { get; set; }
    public double Price { get; set; }
    public bool Status { get; set; } = true;
    public Guid AirlineId { get; set; }

    public Airlines Airline { get; set; }
    public Guid AircraftId { get; set; }

    public Aircraft Aircraft { get; set; }
    public string FlightDuration { get; set; }
}