using Flight.Services.ContextDB;
using Flight.Services.DTOs.FlightDTO;
using Flight.Services.Models;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.Assistant.LocgicHelp;

public class FlightHelper
{
    private readonly ApplicationDBContext _context;
    private readonly OperationResult<GetFlightDTO> _operation;

    public FlightHelper(OperationResult<GetFlightDTO> operation, ApplicationDBContext context)
    {
        _operation = operation;
        _context = context;
    }

    public async Task<bool> FlightCodeExists(int flightCode)
    {
        return await _context.Flights.AnyAsync(x =>
            (x.FlightCode == $"VN-{flightCode}" || x.FlightCode == $"BA-{flightCode}") && x.Status == true);
    }

    public async Task<bool> AirportExists(Guid airportId)
    {
        return await _context.Airports.AnyAsync(x => x.Id == airportId);
    }

    public async Task<Guid> AirlineExists(string role)
    {
        var roleCode = role.ToLower();
        var data = roleCode == "vna"
            ? await _context.Airlines.FirstOrDefaultAsync(x => x.IATA == "VNA")
            : await _context.Airlines.FirstOrDefaultAsync(x => x.IATA == "BBA");

        return data.Id;
    }

    public async Task<bool> AircraftExists(Guid aircraftId)
    {
        return await _context.Aircrafts.AnyAsync(x => x.Id == aircraftId && x.Active == true);
    }

    public Flights MapToFlightEntity(string role, CreateFlightDTO flight)
    {
        return new Flights
        {
            FlightCode = $"{(role.ToLower() == "vna" ? "VN" : "BA")}-{flight.FlightCode}",
            DepartureAirportId = flight.DepartureAirportId,
            ArrivalAirportId = flight.ArrivalAirportId,
            DepartureTime = flight.DepartureTime,
            ArrivalTime = flight.ArrivalTime,
            AvailableSeats = _context.Aircrafts.First(x => x.Id == flight.AircraftId).TotalSeats,
            Price = flight.Price,
            AirlineId = AirlineExists(role).Result,
            AircraftId = flight.AircraftId,
            FlightDuration = (flight.ArrivalTime - flight.DepartureTime).ToString("h':'mm") + " Hours"
        };
    }

    public Flights UpdateFlightEntity(Flights flight, UpdateFlightDTO updateFlight)
    {
        flight.DepartureAirportId = updateFlight.DepartureAirportId ?? flight.DepartureAirportId;
        flight.ArrivalAirportId = updateFlight.ArrivalAirportId ?? flight.ArrivalAirportId;
        flight.DepartureTime = updateFlight.DepartureTime ?? flight.DepartureTime;
        flight.ArrivalTime = updateFlight.ArrivalTime ?? flight.ArrivalTime;
        flight.Price = updateFlight.Price ?? flight.Price;
        flight.AircraftId = updateFlight.AircraftId ?? flight.AircraftId;
        flight.FlightDuration = (flight.ArrivalTime - flight.DepartureTime).ToString("h':'mm") + " Hours";
        flight.AvailableSeats = updateFlight.AircraftId != null
            ? _context.Aircrafts.First(x => x.Id == updateFlight.AircraftId).TotalSeats
            : flight.AvailableSeats;


        return flight;
    }
}