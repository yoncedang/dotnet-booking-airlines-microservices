using Flight.Services.DTOs.AirportDTO;
using Flight.Services.Models;

namespace Flight.Services.Assistant.LocgicHelp;

public class AirportHelper
{
    // Update Airport
    public void UpdateAirportDTO(Airports airports, UpdateAirportDTO updateAirportDTO)
    {
        airports.Name = updateAirportDTO.Name ?? airports.Name;
        airports.City = updateAirportDTO.City ?? airports.City;
        airports.Country = updateAirportDTO.Country ?? airports.Country;
        airports.IATA = updateAirportDTO.IATA ?? airports.IATA;
    }
}