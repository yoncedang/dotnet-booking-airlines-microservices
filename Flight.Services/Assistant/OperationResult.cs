namespace Flight.Services.Assistant;

public class OperationResult<T>
{
    public bool IsSuccess { get; set; }
    public string Message { get; set; }

    public T Data { get; set; }

    // success result
    public OperationResult<T> Success(string message, T data)
    {
        return new OperationResult<T>
        {
            IsSuccess = true,
            Message = message,
            Data = data
        };
    }


    public OperationResult<T> Failed(string message)
    {
        return new OperationResult<T>
        {
            IsSuccess = false,
            Message = message
        };
    }
}