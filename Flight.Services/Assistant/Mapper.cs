using AutoMapper;
using Flight.Services.DTOs.AircraftDTO;
using Flight.Services.DTOs.AirlinesDTO;
using Flight.Services.DTOs.AirportDTO;
using Flight.Services.DTOs.FlightDTO;
using Flight.Services.Models;

namespace Flight.Services.Assistant;

public class ProfileMapper : Profile
{
    public ProfileMapper()
    {
        CreateMap<Airports, CreateAirportDTO>().ReverseMap();
        CreateMap<Airports, GetAirportDTO>().ReverseMap();
        CreateMap<Airports, UpdateAirportDTO>().ReverseMap();
        CreateMap<Airports, ResponseFullFlightDTO>().ReverseMap();


        CreateMap<Flights, CreateFlightDTO>().ReverseMap();
        CreateMap<Flights, GetFlightDTO>().ReverseMap();
        CreateMap<Flights, UpdateFlightDTO>().ReverseMap();
        CreateMap<Flights, ResponseFullFlightDTO>().ReverseMap();


        CreateMap<Aircraft, CreateAircraftDTO>().ReverseMap();
        CreateMap<Aircraft, GetAircraftDTO>().ReverseMap();
        CreateMap<Aircraft, UpdateAircraftDTO>().ReverseMap();
        CreateMap<Aircraft, ResponseFullFlightDTO>().ReverseMap();

        CreateMap<Airlines, GetAirlinesDTO>().ReverseMap();
        CreateMap<Airlines, ResponseFullFlightDTO>().ReverseMap();
    }
}