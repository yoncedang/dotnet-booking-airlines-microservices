using Flight.Services.DTOs.AirportDTO;
using Flight.Services.Repositories.Airport;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flight.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class airportController : ControllerBase
{
    private readonly IAirportRepositories _airportRepositories;

    public airportController(IAirportRepositories airportRepositories)
    {
        _airportRepositories = airportRepositories;
    }

    [HttpPost("create")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public async Task<IActionResult> CreateAirport([FromBody] CreateAirportDTO airport)
    {
        try
        {
            var newAirport = await _airportRepositories.CreateAirport(airport);
            // if Exception
            if (newAirport == null)
                return BadRequest(new
                {
                    messsage = "IATA Already exits"
                });

            return Ok(new
            {
                message = "Create success",
                data = newAirport
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpGet("all")]
    public async Task<IActionResult> GetAllAirport()
    {
        try
        {
            var airports = await _airportRepositories.GetAllAirport();
            return Ok(new
            {
                message = "Get all success",
                data = airports
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }


    [HttpGet("search/{IATA}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, VNA, Bamboo")]
    public async Task<IActionResult> GetAirportById(string IATA)
    {
        try
        {
            var airport = await _airportRepositories.GetAirportByIATA(IATA);
            if (airport == null)
                return NotFound(new
                {
                    message = "Not Found"
                });

            return Ok(new
            {
                message = "Get success",
                data = airport
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }


    [HttpGet("get-one/{id}")]
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, VNA, Bamboo")]
    public async Task<IActionResult> GetAirportById(Guid id)
    {
        try
        {
            var airport = await _airportRepositories.GetAirportByID(id);
            if (airport == null)
                return NotFound(new
                {
                    message = "Not Found"
                });

            return Ok(new
            {
                message = "Get success",
                data = airport
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpDelete("list-id")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public async Task<IActionResult> DeleteListAirport([FromForm] ListDeleteAirportID listID)
    {
        try
        {
            var result = await _airportRepositories.DeleteListAirport(listID);
            if (result)
                return Ok(new
                {
                    message = "Delete success"
                });

            return NotFound(new
            {
                message = "Not Found"
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpPut("change/{id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public async Task<IActionResult> UpdateAirport(Guid id, UpdateAirportDTO updateAirportDTO)
    {
        try
        {
            var result = await _airportRepositories.UpdateAirport(id, updateAirportDTO);
            if (result.IsSuccess)
                return Ok(new
                {
                    success = result.IsSuccess,
                    message = result.Message,
                    data = result.Data
                });

            return BadRequest(new
            {
                success = result.IsSuccess,
                message = result.Message
            });
        }
        catch (Exception e)
        {
            // Return 500 Internal Server Error
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }
}