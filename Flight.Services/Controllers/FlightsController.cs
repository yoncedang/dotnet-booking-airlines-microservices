using System.Security.Claims;
using Flight.Services.DTOs.FlightDTO;
using Flight.Services.Repositories.Flight;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flight.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class flightsController : ControllerBase
{
    private readonly IFlightRepositories _flightRepositories;

    public flightsController(IFlightRepositories flightRepositories)
    {
        _flightRepositories = flightRepositories;
    }

    [HttpPost("create")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "VNA, Bamboo")]
    public async Task<IActionResult> CreateFlight([FromBody] CreateFlightDTO flight)
    {
        try
        {
            var newFlight = await _flightRepositories.CreateFlight(User.FindFirst(ClaimTypes.Role).Value, flight);
            // Take date time not time zone

            if (!newFlight.IsSuccess)
                return BadRequest(new
                {
                    message = newFlight.Message
                });

            return Ok(new
            {
                message = newFlight.Message,
                success = newFlight.IsSuccess,
                data = newFlight.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpPut("update/{Id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "VNA, Bamboo")]
    public async Task<IActionResult> UpdateFlight(Guid Id, [FromBody] UpdateFlightDTO flight)
    {
        try
        {
            var role = User.FindFirst(ClaimTypes.Role).Value;
            var updateFlight =
                await _flightRepositories.UpdateFlight(Id, role, flight);
            if (!updateFlight.IsSuccess)
                return BadRequest(new
                {
                    message = updateFlight.Message
                });

            return Ok(new
            {
                message = updateFlight.Message,
                success = updateFlight.IsSuccess,
                data = updateFlight.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpGet("role-manager")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "VNA, Bamboo")]
    public async Task<IActionResult> GetFlightByRole()
    {
        try
        {
            var role = User.FindFirst(ClaimTypes.Role).Value;
            var flights = await _flightRepositories.GetByRole(role);
            if (flights == null || flights.Count() == 0)
                return NotFound(new
                {
                    message = "Flights not found"
                });

            return Ok(new
            {
                message = "Get flights success",
                success = true,
                data = flights
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpPost("search/one-trip")]
    public async Task<IActionResult> GetOneWayTrip(OneWayDTO oneWay)
    {
        try
        {
            var flights = await _flightRepositories.GetOneWayTrip(oneWay);
            if (!flights.IsSuccess)
                return BadRequest(new
                {
                    message = flights.Message
                });

            if (flights.Data.Count() == 0)
                return NotFound(new
                {
                    message = "Flights not found"
                });

            return Ok(new
            {
                message = flights.Message,
                isSuccess = flights.IsSuccess,
                data = flights.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpPost("search/round-trip")]
    public async Task<IActionResult> GetRoundTrip(RoundTripDTO roundTrip)
    {
        try
        {
            var flights = await _flightRepositories.GetRoundTrip(roundTrip);
            if (!flights.IsSuccess)
                return BadRequest(new
                {
                    message = flights.Message
                });

            return Ok(new
            {
                message = flights.Message,
                isSuccess = flights.IsSuccess,
                data = flights.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpGet("get-one/{Id}")]
    public async Task<IActionResult> GetById(Guid Id)
    {
        try
        {
            var flight = await _flightRepositories.GetById(Id);
            if (!flight.IsSuccess)
                return BadRequest(new
                {
                    message = flight.Message
                });

            return Ok(new
            {
                message = flight.Message,
                isSuccess = flight.IsSuccess,
                data = flight.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpGet("by-code/{code}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "VNA, Bamboo")]
    public async Task<IActionResult> GetByCode(int code)
    {
        try
        {
            var role = User.FindFirst(ClaimTypes.Role).Value;
            var flight = await _flightRepositories.GetByCode(role, code);
            if (!flight.IsSuccess)
                return BadRequest(new
                {
                    message = flight.Message
                });

            return Ok(new
            {
                message = flight.Message,
                isSuccess = flight.IsSuccess,
                data = flight.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }
}