using Flight.Services.DTOs.AircraftDTO;
using Flight.Services.Repositories.Aircraft;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flight.Services.Controllers;

[ApiController]
[Route("api/[controller]")]
public class aircraftController : ControllerBase
{
    private readonly IAircraftRepositories _aircraftRepositories;

    public aircraftController(IAircraftRepositories aircraftRepositories)
    {
        _aircraftRepositories = aircraftRepositories;
    }

    [HttpPost("create")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public async Task<IActionResult> CreateAircraft([FromBody] CreateAircraftDTO aircraft)
    {
        try
        {
            var newAircraft = await _aircraftRepositories.CreateAircraft(aircraft);
            if (!newAircraft.IsSuccess)
                return BadRequest(new
                {
                    message = newAircraft.Message
                });

            return Ok(new
            {
                message = newAircraft.Message,
                success = newAircraft.IsSuccess,
                data = newAircraft.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }


    [HttpGet("all")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, VNA, Bamboo")]
    public async Task<IActionResult> GetAllAircraft()
    {
        try
        {
            var aircrafts = await _aircraftRepositories.GetAllAircraft();
            return Ok(new
            {
                message = "Get all success",
                data = aircrafts
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpGet("{Id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, VNA, Bamboo")]
    public async Task<IActionResult> GetAircraft(Guid Id)
    {
        try
        {
            var aircraft = await _aircraftRepositories.GetAircraft(Id);
            if (!aircraft.IsSuccess)
                return BadRequest(new
                {
                    message = aircraft.Message
                });

            return Ok(new
            {
                message = aircraft.Message,
                success = aircraft.IsSuccess,
                data = aircraft.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpPut("{Id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public async Task<IActionResult> UpdateAircraft(Guid Id, [FromBody] UpdateAircraftDTO aircraft)
    {
        try
        {
            var updateAircraft = await _aircraftRepositories.UpdateAircraft(Id, aircraft);
            if (!updateAircraft.IsSuccess)
                return BadRequest(new
                {
                    message = updateAircraft.Message
                });

            return Ok(new
            {
                message = updateAircraft.Message,
                success = updateAircraft.IsSuccess,
                data = updateAircraft.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }

    [HttpDelete("{Id}")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    // Cookies
    public async Task<IActionResult> DeleteAircraft(Guid Id)
    {
        try
        {
            var deleteAircraft = await _aircraftRepositories.DeleteAircraft(Id);
            if (!deleteAircraft.IsSuccess)
                return BadRequest(new
                {
                    message = deleteAircraft.Message
                });

            return Ok(new
            {
                message = deleteAircraft.Message,
                success = deleteAircraft.IsSuccess,
                data = deleteAircraft.Data
            });
        }
        catch (Exception e)
        {
            return StatusCode(500, new
            {
                err = e.Message,
                message = "Internal Server Error"
            });
        }
    }
}