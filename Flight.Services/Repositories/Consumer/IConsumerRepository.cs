using Flight.Services.ApacheKafka.DataReceiveDTO;
using Flight.Services.Assistant;
using Flight.Services.Models;

namespace Flight.Services.Repositories.Consumer;

public interface IConsumerRepository
{
    // Update seat of flight
    Task<OperationResult<IEnumerable<Flights>>> UpdateSeatFlight(PaymentDTO paymentDTO);

    // Update status of flight payment success
    Task<OperationResult<bool>> ConfirmTicket(SessionDTO sessionDto);


    // Update CancelTicketHangfires
    Task CancelHangfires(SessionDTO sessionDto);

    // Update status of flight payment cancel
    Task<OperationResult<bool>> CancelTicket(SessionDTO sessionDto);
}