using Flight.Services.ApacheKafka.DataReceiveDTO;
using Flight.Services.Assistant;
using Flight.Services.ContextDB;
using Flight.Services.Models;
using Flight.Services.Repositories.Redis;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.Repositories.Consumer;

public class ConsumerRepository : IConsumerRepository
{
    private readonly ApplicationDBContext _context;
    private readonly OperationResult<IEnumerable<Flights>> _operationResult;
    private readonly IRedisRepositories _redis;

    public ConsumerRepository(IRedisRepositories redis, ApplicationDBContext context,
        OperationResult<IEnumerable<Flights>> operationResult)
    {
        _redis = redis;
        _operationResult = operationResult;
        _context = context;
    }

    public async Task<OperationResult<IEnumerable<Flights>>> UpdateSeatFlight(PaymentDTO paymentDTO)
    {
        try
        {
            var allFlights = await _context.Flights.ToListAsync();
            var checkHangFires = await _context.Hangfires.Where(x => x.BasketId == paymentDTO.BasketId).ToListAsync();
            if (checkHangFires != null && checkHangFires.Count > 0)
                return _operationResult.Failed("Ticket has been confirmed");

            if (allFlights == null || allFlights.Count == 0)
                return _operationResult.Failed("Flight not found");

            var listFlights = new List<Flights>();

            foreach (var flight in allFlights)
                if (flight.Id == paymentDTO.FlightComeId || flight.Id == paymentDTO.FlightBackId)
                {
                    flight.AvailableSeats -= paymentDTO.AmountPassenger;
                    listFlights.Add(flight);

                    _context.Hangfires.Add(new Hangfires
                    {
                        FlightId = flight.Id,
                        BasketId = paymentDTO.BasketId,
                        AmountPassenger = paymentDTO.AmountPassenger
                    });

                    Console.WriteLine($"Update seat of flight {flight.Id} successfully");
                }

            await _context.SaveChangesAsync();

            return _operationResult.Success("Update seat of flight successfully", listFlights);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return _operationResult.Failed(e.Message);
        }
    }

    public async Task<OperationResult<bool>> ConfirmTicket(SessionDTO sessionDto)
    {
        try
        {
            var allHangFires = await _context.Hangfires.ToListAsync();

            if (allHangFires == null || allHangFires.Count == 0)
                return new OperationResult<bool>().Failed("Hangfire not found");

            var basketId = sessionDto.metadata.BasketId;
            var flightComeId = sessionDto.metadata.FlightComeId;
            var flightBackId = sessionDto.metadata.FlightBackId;

            foreach (var hangfire in allHangFires)
                if ((hangfire.BasketId == basketId && hangfire.FlightId == flightComeId) ||
                    (hangfire.BasketId == basketId && hangfire.FlightId == flightBackId))
                {
                    Console.WriteLine($"Confirm ticket of flight {hangfire.FlightId} successfully");
                    _context.Hangfires.Remove(hangfire);
                }

            await _context.SaveChangesAsync();

            return new OperationResult<bool>().Success("Confirm ticket successfully", true);
        }
        catch (Exception e)
        {
            return new OperationResult<bool>().Failed(e.Message);
        }
    }

    public async Task CancelHangfires(SessionDTO sessionDto)
    {
        try
        {
            var allHangfires =
                await _context.Hangfires.Where(x => x.BasketId == sessionDto.metadata.BasketId).ToListAsync();

            if (allHangfires == null || allHangfires.Count == 0)
                Console.WriteLine("Hangfire clean!");
            else
                foreach (var hangfire in allHangfires)
                {
                    _context.Hangfires.Remove(hangfire);
                    Console.WriteLine($"Cancel ticket of flight {hangfire.FlightId} successfully");
                }
        }
        catch (Exception e)
        {
            Console.WriteLine("Error in CancelHangfires method: ", e.Message);
        }
    }

    public async Task<OperationResult<bool>> CancelTicket(SessionDTO sessionDto)
    {
        try
        {
            var flightQuery = _context.Flights.AsQueryable();

            if (sessionDto.metadata.FlightBackId != null)
            {
                // Nếu có FlightBackId, tìm theo cả FlightBackId và FlightComeId
                var flightBackId = sessionDto.metadata.FlightBackId.Value;
                var flightComeId = sessionDto.metadata.FlightComeId;

                flightQuery = flightQuery.Where(x => x.Id == flightBackId && x.Id == flightComeId);
            }
            else
            {
                // Nếu không có FlightBackId, tìm theo FlightComeId
                var flightComeId = sessionDto.metadata.FlightComeId;

                flightQuery = flightQuery.Where(x => x.Id == flightComeId);
            }

            var allFlights = await flightQuery.ToListAsync();
            if (allFlights == null || allFlights.Count == 0)
                return new OperationResult<bool>().Failed("Flight not found");

            foreach (var flight in allFlights)
            {
                flight.AvailableSeats += sessionDto.metadata.AmountPassenger;
                Console.WriteLine($"Cancel ticket of flight {flight.Id} successfully");
            }

            await _context.SaveChangesAsync();
            return new OperationResult<bool>().Success("Cancel ticket successfully", true);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error in CancelTicket method: ", e.Message);
            return new OperationResult<bool>().Failed(e.Message);
        }
    }
}