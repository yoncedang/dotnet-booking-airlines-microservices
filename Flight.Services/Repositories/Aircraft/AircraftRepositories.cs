using AutoMapper;
using Flight.Services.Assistant;
using Flight.Services.ContextDB;
using Flight.Services.DTOs.AircraftDTO;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.Repositories.Aircraft;

public class AircraftRepositories : IAircraftRepositories
{
    private readonly ApplicationDBContext _context;
    private readonly IMapper _mapper;
    private readonly OperationResult<GetAircraftDTO> _operationResult;

    public AircraftRepositories(OperationResult<GetAircraftDTO> operationResult, IMapper mapper,
        ApplicationDBContext context)
    {
        _operationResult = operationResult;
        _mapper = mapper;
        _context = context;
    }


    public async Task<OperationResult<GetAircraftDTO>> CreateAircraft(CreateAircraftDTO aircraft)
    {
        var checkModel = await _context.Aircrafts.AnyAsync(x => x.Model.ToLower() == aircraft.Model.ToLower());
        if (checkModel)
            return _operationResult.Failed("Model already exists");

        var newAircraft = _mapper.Map<Models.Aircraft>(aircraft);
        newAircraft.Active = true;
        _context.Aircrafts.Add(newAircraft);
        await _context.SaveChangesAsync();

        var getAircraft = _mapper.Map<GetAircraftDTO>(newAircraft);
        return _operationResult.Success("Create aircraft success", getAircraft);
    }

    public async Task<OperationResult<GetAircraftDTO>> GetAircraft(Guid aircraftId)
    {
        var checkAircraft = await _context.Aircrafts.FindAsync(aircraftId);
        if (checkAircraft == null)
            return _operationResult.Failed("Aircraft does not exist");

        var getAircraft = _mapper.Map<GetAircraftDTO>(checkAircraft);
        return _operationResult.Success("Get aircraft success", getAircraft);
    }

    public async Task<IEnumerable<GetAircraftDTO>> GetAllAircraft()
    {
        var aircrafts = await _context.Aircrafts.ToListAsync();
        return _mapper.Map<IEnumerable<GetAircraftDTO>>(aircrafts);
    }

    public async Task<OperationResult<GetAircraftDTO>> UpdateAircraft(Guid aircraftId, UpdateAircraftDTO aircraft)
    {
        var checkId = await _context.Aircrafts.FindAsync(aircraftId);

        if (checkId == null)
            return _operationResult.Failed("Aircraft does not exist");

        var checkModel = await _context.Aircrafts.AnyAsync(x => x.Model.ToLower() == aircraft.Model.ToLower());
        if (checkModel)
            return _operationResult.Failed("Model already exists");

        var UpdateAircraft = new Models.Aircraft
        {
            Name = aircraft.Name ?? checkId.Name,
            Model = aircraft.Model ?? checkId.Model,
            TotalSeats = aircraft.TotalSeats ?? checkId.TotalSeats,
            Active = aircraft.Active ?? checkId.Active
        };

        _context.Aircrafts.Update(UpdateAircraft);
        await _context.SaveChangesAsync();

        var getAircraft = _mapper.Map<GetAircraftDTO>(UpdateAircraft);
        return _operationResult.Success("Update aircraft success", getAircraft);
    }


    public async Task<OperationResult<GetAircraftDTO>> DeleteAircraft(Guid aircraftId)
    {
        var checkAircraft = await _context.Aircrafts.FindAsync(aircraftId);
        if (checkAircraft == null)
            return _operationResult.Failed("Aircraft does not exist");

        _context.Aircrafts.Remove(checkAircraft);
        await _context.SaveChangesAsync();

        var getAircraft = _mapper.Map<GetAircraftDTO>(checkAircraft);
        return _operationResult.Success("Delete aircraft success", getAircraft);
    }
}