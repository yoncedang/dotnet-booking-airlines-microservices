using Flight.Services.Assistant;
using Flight.Services.DTOs.AircraftDTO;

namespace Flight.Services.Repositories.Aircraft;

public interface IAircraftRepositories
{
    // Create
    Task<OperationResult<GetAircraftDTO>> CreateAircraft(CreateAircraftDTO aircraft);

    // Read
    Task<OperationResult<GetAircraftDTO>> GetAircraft(Guid aircraftId);

    Task<IEnumerable<GetAircraftDTO>> GetAllAircraft();

    // Update
    Task<OperationResult<GetAircraftDTO>> UpdateAircraft(Guid aircraftId, UpdateAircraftDTO aircraft);

    // Delete
    Task<OperationResult<GetAircraftDTO>> DeleteAircraft(Guid aircraftId);
}