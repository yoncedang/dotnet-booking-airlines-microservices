using Flight.Services.Assistant;
using Flight.Services.DTOs.FlightDTO;

namespace Flight.Services.Repositories.Flight;

public interface IFlightRepositories
{
    // Create
    Task<OperationResult<GetFlightDTO>> CreateFlight(string role, CreateFlightDTO flight);

    // Update
    Task<OperationResult<GetFlightDTO>> UpdateFlight(Guid Id, string Role, UpdateFlightDTO flight);

    // Get by Role
    Task<IEnumerable<ResponseFullFlightDTO>> GetByRole(string role);

    // Get một chiều
    Task<OperationResult<IEnumerable<ResponseFullFlightDTO>>> GetOneWayTrip(OneWayDTO oneWay);

    // Get khứ hồi
    Task<OperationResult<FullRoundTripDTO>> GetRoundTrip(RoundTripDTO roundTrip);


    // Get by Id
    Task<OperationResult<ResponseFullFlightDTO>> GetById(Guid Id);

    // Get by Code
    Task<OperationResult<ResponseFullFlightDTO>> GetByCode(string role, int code);

    // HangFire Auto update status
    Task AutoUpdateStatus();

    Task HangfiresExpired();
}