using AutoMapper;
using Flight.Services.Assistant;
using Flight.Services.Assistant.LocgicHelp;
using Flight.Services.ContextDB;
using Flight.Services.DTOs.FlightDTO;
using Flight.Services.Repositories.Redis;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.Repositories.Flight;

public class FlightRepositories : IFlightRepositories
{
    private readonly ApplicationDBContext _context;
    private readonly FlightHelper _flightHelper;
    private readonly IMapper _mapper;
    private readonly OperationResult<GetFlightDTO> _operationResult;
    private readonly IRedisRepositories _redis;
    private readonly OperationResult<IEnumerable<ResponseFullFlightDTO>> _responseFullFlight;
    private readonly OperationResult<FullRoundTripDTO> _roundTrip;

    public FlightRepositories(ApplicationDBContext context, IRedisRepositories redis, IMapper mapper,
        OperationResult<GetFlightDTO> operationResult, FlightHelper flightHelper,
        OperationResult<IEnumerable<ResponseFullFlightDTO>> operation,
        OperationResult<FullRoundTripDTO> roundTrip)
    {
        _context = context;
        _redis = redis;
        _mapper = mapper;
        _operationResult = operationResult;
        _flightHelper = flightHelper;
        _responseFullFlight = operation;

        _roundTrip = roundTrip;
    }

    public async Task<OperationResult<GetFlightDTO>> CreateFlight(string role, CreateFlightDTO flight)
    {
        if (await _flightHelper.FlightCodeExists(flight.FlightCode))
            return _operationResult.Failed("Flight code already exists");

        if (!await _flightHelper.AirportExists(flight.DepartureAirportId) ||
            !await _flightHelper.AirportExists(flight.ArrivalAirportId))
            return _operationResult.Failed("Departure or arrival airport does not exist");

        if (flight.DepartureTime < DateTime.Now || flight.ArrivalTime < flight.DepartureTime)
            return _operationResult.Failed("Invalid departure or arrival time");

        if (!await _flightHelper.AircraftExists(flight.AircraftId))
            return _operationResult.Failed("Aircraft does not exist");

        var newFlight = _flightHelper.MapToFlightEntity(role, flight);

        _context.Flights.Add(newFlight);
        await _context.SaveChangesAsync();


        var getFlight = _mapper.Map<GetFlightDTO>(newFlight);

        var FlightById = await GetById(newFlight.Id);
        await _redis.SetAsync(newFlight.Id.ToString(), new
        {
            Flight = FlightById.Data
        }, flight.ArrivalTime - DateTime.Now);

        Console.WriteLine("Create flight success");
        return _operationResult.Success("Create flight success", getFlight);
    }


    public async Task<OperationResult<GetFlightDTO>> UpdateFlight(Guid Id, string role, UpdateFlightDTO flight)
    {
        try
        {
            var checkId = await _context.Flights.FindAsync(Id);
            if (checkId == null)
                return _operationResult.Failed("Flight does not exist");

            // Kiểm tra DepartureAirportId và ArrivalAirportId có tồn tại không
            if ((flight.DepartureAirportId != null &&
                 !await _flightHelper.AirportExists(flight.DepartureAirportId.Value)) ||
                (flight.ArrivalAirportId != null && !await _flightHelper.AirportExists(flight.ArrivalAirportId.Value)))
                return _operationResult.Failed("Departure or arrival airport does not exist");

            // Kiểm tra DepartureTime và ArrivalTime có hợp lệ không
            if ((flight.DepartureTime != null && flight.DepartureTime < DateTime.Now) ||
                (flight.ArrivalTime != null && flight.ArrivalTime <= flight.DepartureTime))
                return _operationResult.Failed("Invalid departure or arrival time");

            // Kiểm tra AircraftId có tồn tại không
            if (flight.AircraftId != null && !await _flightHelper.AircraftExists(flight.AircraftId.Value))
                return _operationResult.Failed("Aircraft does not exist");


            var updateData = _flightHelper.UpdateFlightEntity(checkId, flight);
            await _context.SaveChangesAsync();

            var FlightById = await GetById(updateData.Id);
            await _redis.UpdateAsync(updateData.Id.ToString(), new
            {
                Flight = FlightById.Data
            }, flight.ArrivalTime - DateTime.Now ?? null);

            var getFlight = _mapper.Map<GetFlightDTO>(updateData);
            return _operationResult.Success("Update flight success", getFlight);
        }
        catch (Exception ex)
        {
            // Log or handle the exception appropriately
            return _operationResult.Failed($"An error occurred while updating the flight: {ex.Message}");
        }
    }

    public async Task<IEnumerable<ResponseFullFlightDTO>> GetByRole(string role)
    {
        var identityRole = role.ToLower() == "vna" ? "VN" : "BA";

        // Lấy danh sách chuyến bay theo role va include các bảng liên quan
        var data = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.FlightCode.Contains(identityRole))
            .ToListAsync();

        return _mapper.Map<IEnumerable<ResponseFullFlightDTO>>(data);
    }

    public async Task<OperationResult<IEnumerable<ResponseFullFlightDTO>>> GetOneWayTrip(OneWayDTO oneWay)
    {
        var checkDepartureAirport = await _context.Airports.FindAsync(oneWay.DepartureAirportId);
        var checkArrivalAirport = await _context.Airports.FindAsync(oneWay.ArrivalAirportId);

        if (checkDepartureAirport == null || checkArrivalAirport == null)
            return _responseFullFlight.Failed("Departure or arrival airport does not exist");

        var data = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.DepartureTime.Date == oneWay.Date &&
                        x.AvailableSeats >= oneWay.QuantityTickets &&
                        x.DepartureTime.DateTime > DateTime.Now &&
                        x.DepartureAirportId == oneWay.DepartureAirportId &&
                        x.ArrivalAirportId == oneWay.ArrivalAirportId)
            .ToListAsync();

        var result = _mapper.Map<IEnumerable<ResponseFullFlightDTO>>(data);
        return _responseFullFlight.Success("Get flights success", result);
    }

    public async Task<OperationResult<FullRoundTripDTO>> GetRoundTrip(RoundTripDTO roundTrip)
    {
        var checkDepartureAirport = await _context.Airports.FindAsync(roundTrip.DepartureAirportId);
        var checkArrivalAirport = await _context.Airports.FindAsync(roundTrip.ArrivalAirportId);

        if (checkDepartureAirport == null || checkArrivalAirport == null)
            return _roundTrip.Failed("Departure or arrival airport does not exist");

        var FirstFlight = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.DepartureTime.Date == roundTrip.DateStart &&
                        x.AvailableSeats >= roundTrip.QuantityTickets &&
                        x.DepartureTime.DateTime > DateTime.Now &&
                        x.DepartureAirportId == roundTrip.DepartureAirportId &&
                        x.ArrivalAirportId == roundTrip.ArrivalAirportId)
            .ToListAsync();

        var SecondFlight = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.DepartureTime.Date == roundTrip.DateBack &&
                        x.AvailableSeats >= roundTrip.QuantityTickets &&
                        x.DepartureTime.DateTime > DateTime.Now &&
                        x.ArrivalAirportId == roundTrip.DepartureAirportId &&
                        x.DepartureAirportId == roundTrip.ArrivalAirportId)
            .ToListAsync();

        var result = new FullRoundTripDTO
        {
            FirstFlight = _mapper.Map<IEnumerable<ResponseFullFlightDTO>>(FirstFlight),
            SecondFlight = _mapper.Map<IEnumerable<ResponseFullFlightDTO>>(SecondFlight)
        };

        return _roundTrip.Success("Get flights success", result);
    }

    public async Task<OperationResult<ResponseFullFlightDTO>> GetById(Guid Id)
    {
        var results = new OperationResult<ResponseFullFlightDTO>();
        var checkID = await _context.Flights.FindAsync(Id);
        if (checkID == null)
            return results.Failed("Flight does not exist");

        var data = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.Id == Id)
            .FirstOrDefaultAsync();

        var result = _mapper.Map<ResponseFullFlightDTO>(data);
        return results.Success("Get flight success", result);
    }

    public async Task<OperationResult<ResponseFullFlightDTO>> GetByCode(string role, int code)
    {
        var results = new OperationResult<ResponseFullFlightDTO>();
        var roleCode = role.ToLower() == "vna" ? "VN" : "BA";
        var data = await _context.Flights
            .Include(x => x.DepartureAirport)
            .Include(x => x.ArrivalAirport)
            .Include(x => x.Airline)
            .Include(x => x.Aircraft)
            .Where(x => x.FlightCode == $"{roleCode}-{code}")
            .FirstOrDefaultAsync();

        if (data == null)
            return results.Failed("Flight does not exist");

        var result = _mapper.Map<ResponseFullFlightDTO>(data);
        return results.Success("Get flight success", result);
    }

    public async Task AutoUpdateStatus()
    {
        var checkData = await _context.Flights.Where(x => x.DepartureTime <= DateTime.Now).ToListAsync();
        foreach (var item in checkData)
        {
            // await _redis.DeleteAsync(item.Id.ToString());
            item.Status = false;
        }

        await _context.SaveChangesAsync();

        Console.WriteLine("Auto update status success");
    }

    public async Task HangfiresExpired()
    {
        Console.WriteLine("Hangfire is running...");
        var currentTime = DateTime.UtcNow;
        Console.WriteLine(currentTime);

        var checkHangfires = await _context.Hangfires.ToListAsync();
        foreach (var basket in checkHangfires)
            if (basket.Expired <= currentTime)
            {
                var flight = await _context.Flights.FindAsync(basket.FlightId);
                flight.AvailableSeats += basket.AmountPassenger;
                _context.Hangfires.Remove(basket);

                Console.WriteLine($"Hangfire expired, flight {flight.Id} has been updated");
            }

        await _context.SaveChangesAsync();
    }
}