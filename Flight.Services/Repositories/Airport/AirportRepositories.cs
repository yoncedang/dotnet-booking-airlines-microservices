using AutoMapper;
using Flight.Services.Assistant;
using Flight.Services.Assistant.LocgicHelp;
using Flight.Services.ContextDB;
using Flight.Services.DTOs.AirportDTO;
using Flight.Services.Models;
using Microsoft.EntityFrameworkCore;

namespace Flight.Services.Repositories.Airport;

public class AirportRepositories : IAirportRepositories
{
    private readonly AirportHelper _airportHelper;
    private readonly ApplicationDBContext _context;
    private readonly IMapper _mapper;

    public AirportRepositories(ApplicationDBContext context, IMapper mapper, AirportHelper airportHelper)
    {
        _context = context;
        _mapper = mapper;
        _airportHelper = airportHelper;
    }


    public async Task<GetAirportDTO> CreateAirport(CreateAirportDTO airport)
    {
        var checkIATA = await _context.Airports.FirstOrDefaultAsync(x => x.IATA == airport.IATA);
        if (checkIATA != null)
            return null;
        var newAirport = _mapper.Map<Airports>(airport);
        await _context.Airports.AddAsync(newAirport);
        await _context.SaveChangesAsync();

        var resultsMap = _mapper.Map<GetAirportDTO>(newAirport);
        return resultsMap;
    }

    public async Task<IEnumerable<GetAirportDTO>> GetAllAirport()
    {
        var airports = await _context.Airports.ToListAsync();
        var resultsMap = _mapper.Map<IEnumerable<GetAirportDTO>>(airports);
        return resultsMap;
    }

    public async Task<GetAirportDTO> GetAirportByIATA(string IATA)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(x => x.IATA.ToUpper() == IATA.ToUpper());
        var resultsMap = _mapper.Map<GetAirportDTO>(airport);
        return resultsMap;
    }

    public async Task<GetAirportDTO> GetAirportByID(Guid id)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(x => x.Id == id);
        var resultsMap = _mapper.Map<GetAirportDTO>(airport);
        return resultsMap;
    }

    public async Task<bool> DeleteListAirport(ListDeleteAirportID listID)
    {
        var listAirport = await _context.Airports.ToListAsync();

        foreach (var item in listID.Id)
        {
            var airport = listAirport.FirstOrDefault(x => x.Id == item);
            if (airport == null) continue;

            _context.Airports.Remove(airport);
        }

        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<OperationResult<GetAirportDTO>> UpdateAirport(Guid id, UpdateAirportDTO updateAirportDTO)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(x => x.Id == id);
        if (airport == null)
            return new OperationResult<GetAirportDTO>().Failed("Airport not found");

        var checkIATA = await _context.Airports.FirstOrDefaultAsync(x => x.IATA == updateAirportDTO.IATA);
        if (checkIATA != null)
            return new OperationResult<GetAirportDTO>().Failed("IATA already exists");

        _airportHelper.UpdateAirportDTO(airport, updateAirportDTO);
        await _context.SaveChangesAsync();

        var resultsMap = _mapper.Map<GetAirportDTO>(airport);
        return new OperationResult<GetAirportDTO>().Success("Update success", resultsMap);
    }
}