using Flight.Services.Assistant;
using Flight.Services.DTOs.AirportDTO;

namespace Flight.Services.Repositories.Airport;

public interface IAirportRepositories
{
    Task<GetAirportDTO> CreateAirport(CreateAirportDTO airport);

    Task<IEnumerable<GetAirportDTO>> GetAllAirport();

    Task<GetAirportDTO> GetAirportByIATA(string IATA);

    Task<GetAirportDTO> GetAirportByID(Guid id);

    // Delete List Airport
    Task<bool> DeleteListAirport(ListDeleteAirportID listID);

    // Update Airport
    Task<OperationResult<GetAirportDTO>> UpdateAirport(Guid id, UpdateAirportDTO updateAirportDTO);
}