using System.Text.Json;
using StackExchange.Redis;

namespace Flight.Services.Repositories.Redis;

public class RedisRepositories : IRedisRepositories
{
    private readonly IDatabase _db;

    public RedisRepositories(IConnectionMultiplexer redis)
    {
        _db = redis.GetDatabase();
    }

    public async Task<T> GetAsync<T>(string _key)
    {
        try
        {
            var data = await _db.StringGetAsync(_key);
            if (data.IsNullOrEmpty) return default;

            var result = JsonSerializer.Deserialize<T>(data);
            return result;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return default;
        }
    }

    public async Task SetAsync<T>(string _key, T? value, TimeSpan? expiry = null)
    {
        try
        {
            Console.WriteLine("SetAsync 1 ");
            var data = JsonSerializer.Serialize(value);
            await _db.StringSetAsync(_key, data, expiry);
            Console.WriteLine("SetAsync 2");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public async Task<bool> ExistsAsync(string _key)
    {
        try
        {
            return _db.KeyExists(_key);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }

    public async Task<bool> DeleteAsync(string _key)
    {
        try
        {
            var check = _db.KeyExists(_key);
            if (check) return _db.KeyDelete(_key) || await _db.KeyDeleteAsync(_key);

            return false;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }

    public async Task<bool> UpdateAsync<T>(string key, T value, TimeSpan? expiry = null)
    {
        try
        {
            var check = _db.KeyExists(key);

            if (check)
            {
                await _db.KeyDeleteAsync(key);
                await SetAsync(key, value, expiry);
                return true;
            }

            return false;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }
}