using Confluent.Kafka;

namespace Flight.Services.ApacheKafka.Consumer;

public class KafkaGroupId : IKafkaGroupdId
{
    private readonly IConfiguration _configuration;

    public KafkaGroupId(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public ConsumerConfig HoldingTicketGroup()
    {
        var config = new ConsumerConfig
        {
            BootstrapServers = _configuration["Kafka:BootstrapServers"],
            GroupId = "Flight.Services.HoldingTicket",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnableAutoCommit = false
        };

        return config;
    }

    public ConsumerConfig PaymentSuccessGroup()
    {
        var config = new ConsumerConfig
        {
            BootstrapServers = _configuration["Kafka:BootstrapServers"],
            GroupId = "Flight.Services.PaymentSuccess",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnableAutoCommit = false
        };

        return config;
    }

    public ConsumerConfig PaymentCancelGroup()
    {
        var config = new ConsumerConfig
        {
            BootstrapServers = _configuration["Kafka:BootstrapServers"],
            GroupId = "Flight.Services.PaymentCancel",
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnableAutoCommit = false
        };

        return config;
    }
}