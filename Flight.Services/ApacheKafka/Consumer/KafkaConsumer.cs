using Confluent.Kafka;
using Flight.Services.ApacheKafka.Consumer;
using Flight.Services.ApacheKafka.DataReceiveDTO;
using Flight.Services.Repositories.Consumer;
using Flight.Services.Repositories.Redis;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Payment.Services.ApacheKafka;

public class KafkaConsumer : IHostedService, IKafkaConsumer
{
    private readonly IConsumerRepository _consumerRepository;
    private readonly IConsumer<string, string> _holdingTicketConsumer;
    private readonly IKafkaGroupdId _kafkaGroupdId;
    private readonly IConsumer<string, string> _paymentCancelConsumer;

    private readonly IConsumer<string, string> _paymentSuccessConsumer;
    // private readonly IConsumerRepository _consumerRepository;

    private readonly IRedisRepositories _redis;


    public KafkaConsumer(IRedisRepositories redis, IConsumerRepository consumerRepository, IKafkaGroupdId kafkaGroupdId)
    {
        _redis = redis;
        _consumerRepository = consumerRepository;
        _kafkaGroupdId = kafkaGroupdId;

        _holdingTicketConsumer = new ConsumerBuilder<string, string>(_kafkaGroupdId.HoldingTicketGroup()).Build();
        _paymentSuccessConsumer = new ConsumerBuilder<string, string>(_kafkaGroupdId.PaymentSuccessGroup()).Build();
        _paymentCancelConsumer = new ConsumerBuilder<string, string>(_kafkaGroupdId.PaymentCancelGroup()).Build();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _holdingTicketConsumer.Subscribe("HoldingTicket");
        _paymentSuccessConsumer.Subscribe("PaymentSuccess");
        _paymentCancelConsumer.Subscribe("PaymentCancel");

        await Task.WhenAll(
            Task.Run(() => ExecuteAsync(_holdingTicketConsumer, cancellationToken), cancellationToken),
            Task.Run(() => ExecuteAsync(_paymentSuccessConsumer, cancellationToken), cancellationToken),
            Task.Run(() => ExecuteAsync(_paymentCancelConsumer, cancellationToken), cancellationToken)
        );
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _holdingTicketConsumer.Close();
        _paymentSuccessConsumer.Close();
        _paymentCancelConsumer.Close();
    }


    public async Task ExecuteAsync(IConsumer<string, string> _consumer, CancellationToken stoppingToken)
    {
        try
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    Console.WriteLine("Kafka Consumer Flight.Services is listening...");
                    var consumeResult = _consumer.Consume(stoppingToken);
                    var key = consumeResult.Message.Key;
                    var topic = consumeResult.Topic;
                    var value = consumeResult.Message.Value;

                    switch (topic)
                    {
                        case "HoldingTicket":
                            Console.WriteLine("Case HoldingTicket");
                            Console.WriteLine("Key: " + consumeResult.Message.Key);
                            var checkKey = await _redis.ExistsAsync(consumeResult.Message.Key);
                            if (!checkKey)
                            {
                                Console.WriteLine("Key not found");
                                break;
                            }

                            var receivePaymentDto =
                                JsonSerializer.Deserialize<PaymentDTO>(value);
                            Console.WriteLine("FlightComeId: " + receivePaymentDto.FlightComeId);
                            var result = await _consumerRepository.UpdateSeatFlight(receivePaymentDto);
                            if (result.IsSuccess)
                            {
                                Console.WriteLine(result.Message);
                                foreach (var flight in result.Data) Console.WriteLine(flight.Id);

                                await _redis.DeleteAsync(consumeResult.Message.Key);
                                Console.WriteLine("DeletedKey: " + consumeResult.Message.Key);
                                _consumer.Commit();
                                Console.WriteLine("Commited");
                                break;
                            }

                            Console.WriteLine(result.Message);
                            break;
                        case "PaymentSuccess":
                            Console.WriteLine("Case PaymentSuccess");
                            Console.WriteLine("Key: " + consumeResult.Message.Key);
                            var checkKeyExits = await _redis.ExistsAsync(consumeResult.Message.Key);
                            if (!checkKeyExits)
                            {
                                Console.WriteLine("Key not found");
                                break;
                            }

                            var receiveSessionDTO =
                                JsonSerializer.Deserialize<SessionDTO>(value);
                            Console.WriteLine("FlightComeId: " + receiveSessionDTO.metadata.FlightComeId);

                            var resultPayment = await _consumerRepository.ConfirmTicket(receiveSessionDTO);
                            if (resultPayment.IsSuccess)
                            {
                                Console.WriteLine(resultPayment.Message);
                                await _redis.DeleteAsync(consumeResult.Message.Key);
                                Console.WriteLine("DeletedKey: " + consumeResult.Message.Key);
                                _consumer.Commit();
                                break;
                            }

                            Console.WriteLine(resultPayment.Message);
                            break;
                        case "PaymentCancel":
                            Console.WriteLine("Case PaymentCancel");
                            Console.WriteLine("Key: " + consumeResult.Message.Key);

                            var checkKeyExitsCancel = await _redis.ExistsAsync(consumeResult.Message.Key);
                            if (!checkKeyExitsCancel)
                            {
                                Console.WriteLine("Key not found");
                                break;
                            }

                            var receiveSessionDTOCancel =
                                JsonSerializer.Deserialize<SessionDTO>(value);

                            Console.WriteLine("FlightComeId: " + receiveSessionDTOCancel.metadata.FlightComeId);

                            await _consumerRepository.CancelHangfires(receiveSessionDTOCancel);

                            var resultPaymentCancel = await _consumerRepository.CancelTicket(receiveSessionDTOCancel);
                            if (resultPaymentCancel.IsSuccess)
                            {
                                Console.WriteLine(resultPaymentCancel.Message);
                                await _redis.DeleteAsync(consumeResult.Message.Key);
                                Console.WriteLine("DeletedKey: " + consumeResult.Message.Key);
                                _consumer.Commit();
                                break;
                            }

                            Console.WriteLine(resultPaymentCancel.Message);
                            break;

                        default:
                            Console.WriteLine($"Unhandled key: {topic}");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine(e.Message);
        }
        finally
        {
            _consumer.Close();
        }
    }
}