using Confluent.Kafka;

namespace Flight.Services.ApacheKafka.Consumer;

public interface IKafkaGroupdId
{
    // HoldingTicketGroup
    ConsumerConfig HoldingTicketGroup();
    ConsumerConfig PaymentSuccessGroup();

    ConsumerConfig PaymentCancelGroup();
}