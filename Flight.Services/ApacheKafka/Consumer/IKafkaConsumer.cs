using Confluent.Kafka;

namespace Payment.Services.ApacheKafka;

public interface IKafkaConsumer
{
    Task StartAsync(CancellationToken cancellationToken);
    Task StopAsync(CancellationToken cancellationToken);
    Task ExecuteAsync(IConsumer<string, string> _consumer, CancellationToken stoppingToken);
}