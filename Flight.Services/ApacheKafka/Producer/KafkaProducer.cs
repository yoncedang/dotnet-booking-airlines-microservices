using System.Text.Json;
using Confluent.Kafka;
using Flight.Services.Repositories.Redis;

namespace Flight.Services.ApacheKafka.Producer;

public class KafkaProducer : IKafkaProducer
{
    private readonly IProducer<string, string> _producer;

    // private readonly ProducerConfig _producerConfig;
    private readonly IRedisRepositories _redisRepositories;

    public KafkaProducer(IRedisRepositories redisRepositories, IConfiguration configuration)
    {
        _redisRepositories = redisRepositories;
        var config = new ProducerConfig
        {
            BootstrapServers = configuration["Kafka:BootstrapServers"],
            ClientId = "PaymentGateway.Services",
            AllowAutoCreateTopics = true,
            EnableIdempotence = true
        };

        _producer = new ProducerBuilder<string, string>(config).Build();
    }

    public async Task SendMessage(string topic, string key, string message)
    {
        try
        {
            await _redisRepositories.SetAsync(key, message);
            var result = await _producer.ProduceAsync(topic, new Message<string, string>
            {
                Key = key,
                Value = message
            });
            // Check Message is sent
            Console.WriteLine($"Message: {message} sent to {result.TopicPartitionOffset}");
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Delivery failed: {e.Error.Reason}");
        }
    }

    public async Task SendMessageGeneric<T>(string topic, string key, T message)
    {
        try
        {
            var data = JsonSerializer.Serialize(message);
            await _redisRepositories.SetAsync(key, data);
            Console.WriteLine($"Data: {data}");
            var result = await _producer.ProduceAsync(topic, new Message<string, string>
            {
                Key = key,
                Value = data
            });
            // Check Message is sent
            Console.WriteLine($"Message: {message} sent to {result.TopicPartitionOffset}");
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Delivery failed: {e.Error.Reason}");
        }
    }
}