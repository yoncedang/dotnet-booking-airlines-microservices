namespace Flight.Services.ApacheKafka.Producer;

public interface IKafkaProducer
{
    Task SendMessageGeneric<T>(string topic, string key, T message);
    Task SendMessage(string topic, string key, string message);
}