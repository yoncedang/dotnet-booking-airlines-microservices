namespace Flight.Services.ApacheKafka.DataReceiveDTO;

public class SessionDTO
{
    public MetaData metadata { get; set; }
    public string paymentStatus { get; set; }
}

public class MetaData
{
    public int AmountPassenger { get; set; }
    public int AmountTicket { get; set; }
    public Guid BasketId { get; set; }
    public Guid? FlightBackId { get; set; }
    public Guid FlightComeId { get; set; }
    public bool IsPending { get; set; }
    public double Price { get; set; }
    public string Status { get; set; }
    public Guid UserId { get; set; }
}