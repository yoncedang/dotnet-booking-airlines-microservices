﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Flight.Services.Migrations
{
    /// <inheritdoc />
    public partial class V1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aircrafts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Model = table.Column<string>(type: "text", nullable: false),
                    TotalSeats = table.Column<int>(type: "integer", nullable: false),
                    Active = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aircrafts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Airlines",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Country = table.Column<string>(type: "text", nullable: false),
                    IATA = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airlines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Airports",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    City = table.Column<string>(type: "text", nullable: false),
                    Country = table.Column<string>(type: "text", nullable: false),
                    IATA = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hangfires",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FlightId = table.Column<Guid>(type: "uuid", nullable: false),
                    BasketId = table.Column<Guid>(type: "uuid", nullable: false),
                    AmountPassenger = table.Column<int>(type: "integer", nullable: false),
                    Expired = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hangfires", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Flights",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FlightCode = table.Column<string>(type: "text", nullable: false),
                    DepartureAirportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ArrivalAirportId = table.Column<Guid>(type: "uuid", nullable: false),
                    DepartureTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ArrivalTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    AvailableSeats = table.Column<int>(type: "integer", nullable: false),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    Status = table.Column<bool>(type: "boolean", nullable: false),
                    AirlineId = table.Column<Guid>(type: "uuid", nullable: false),
                    AircraftId = table.Column<Guid>(type: "uuid", nullable: false),
                    FlightDuration = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flights", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Flights_Aircrafts_AircraftId",
                        column: x => x.AircraftId,
                        principalTable: "Aircrafts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Flights_Airlines_AirlineId",
                        column: x => x.AirlineId,
                        principalTable: "Airlines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Flights_Airports_ArrivalAirportId",
                        column: x => x.ArrivalAirportId,
                        principalTable: "Airports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Flights_Airports_DepartureAirportId",
                        column: x => x.DepartureAirportId,
                        principalTable: "Airports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Aircrafts",
                columns: new[] { "Id", "Active", "Model", "Name", "TotalSeats" },
                values: new object[,]
                {
                    { new Guid("0dbaad64-0ae2-4a14-a85e-233c241c5c06"), true, "B787", "Boeing", 250 },
                    { new Guid("7272d7f5-2d4d-440a-9a6e-0c3a302add38"), true, "B777", "Boeing", 300 },
                    { new Guid("92429754-1d4a-4b52-8c07-907ff75746b2"), true, "A321", "Airbus", 220 },
                    { new Guid("9aa96b00-6199-4f8a-bc96-89068cb86686"), true, "A320", "Airbus", 190 },
                    { new Guid("e191965e-4e65-4800-ab5e-82cf605fbbc5"), true, "A350", "Airbus", 300 },
                    { new Guid("f724928c-f190-4372-853e-65ab5d89d06f"), true, "B737", "Boeing", 250 }
                });

            migrationBuilder.InsertData(
                table: "Airlines",
                columns: new[] { "Id", "Country", "IATA", "Name" },
                values: new object[,]
                {
                    { new Guid("75f55142-d902-4762-a2d2-fc49bae9e497"), "Vietnam", "BBA", "Bamboo Airways" },
                    { new Guid("af65c7b5-fba8-4c3f-bceb-b09d9cc24c01"), "Vietnam", "VNA", "Vietnam Airlines" }
                });

            migrationBuilder.InsertData(
                table: "Airports",
                columns: new[] { "Id", "City", "Country", "IATA", "Name" },
                values: new object[,]
                {
                    { new Guid("0fbb9366-9981-486a-94cc-7ca4b104c562"), "Cà Mau", "Việt Nam", "CAH", "Cà Mau Airport" },
                    { new Guid("251acd6a-45a4-40c3-989f-3f2b1fd3a09b"), "Quảng Bình", "Việt Nam", "VDH", "Đồng Hới Airport" },
                    { new Guid("2cd1e38b-c9f2-4160-aef6-0b3379ac5573"), "Đà Lạt", "Việt Nam", "DLI", "Liên Khương Airport" },
                    { new Guid("301f9a6a-59a2-4bef-8a16-79a52a5df717"), "Vân Đồn", "Việt Nam", "VDO", "Vân Đồn International Airport" },
                    { new Guid("339abdf0-d9c6-4137-9463-5972deddc102"), "Hải Phòng", "Việt Nam", "HPH", "Cát Bi International Airport" },
                    { new Guid("498cc85d-635d-43ce-9e56-7184a146de66"), "Điện Biên", "Việt Nam", "DIN", "Điện Biên Phủ Airport" },
                    { new Guid("4e6ee996-f19f-442d-8922-9893a443d123"), "Nha Trang", "Việt Nam", "CXR", "Cam Ranh International Airport" },
                    { new Guid("581789ed-5753-4ec5-9f47-75e6a2a88b8c"), "Pleiku", "Việt Nam", "PXU", "Pleiku Airport" },
                    { new Guid("5e6a214b-d3c3-4d1b-befa-4f51a781dbf2"), "Hồ Chí Minh", "Việt Nam", "SGN", "Tân Sơn Nhất International Airport" },
                    { new Guid("67dde5da-81fc-465f-96c4-40398bf481f2"), "Côn Đảo", "Việt Nam", "VCS", "Côn Đảo Airport" },
                    { new Guid("6935ca6a-dd17-44eb-8020-80780f50136b"), "Thanh Hóa", "Việt Nam", "THD", "Thọ Xuân Airport" },
                    { new Guid("6d5f8020-fd81-445a-bf7e-a7dd10916ba5"), "Phú Yên", "Việt Nam", "TBB", "Tuy Hòa Airport" },
                    { new Guid("9d53dc3f-3f95-4a3d-a363-3369f6b04fbd"), "Phú Quốc", "Việt Nam", "PQC", "Phú Quốc International Airport" },
                    { new Guid("ba64725d-5aee-4991-a910-d0a42fb04832"), "Quảng Nam", "Việt Nam", "VCL", "Chu Lai International Airport" },
                    { new Guid("bf3ed355-ea13-4cbb-96e0-02e5cab8e433"), "Cần Thơ", "Việt Nam", "VCA", "Cần Thơ International Airport" },
                    { new Guid("cbae1eb9-4bba-481a-a24c-991253d85ff4"), "Hà Nội", "Việt Nam", "HAN", "Nội Bài International Airport" },
                    { new Guid("d5bebe66-c703-423e-94a8-d606ed9934b0"), "Ban Mê Thuột", "Việt Nam", "BMV", "Buôn Ma Thuột Airport" },
                    { new Guid("d644d71f-d0fe-411e-9feb-262f746b5c5c"), "Qui Nhơn", "Việt Nam", "UIH", "Phù Cát Airport" },
                    { new Guid("de5f8a49-321a-4964-bb47-c23d70490e79"), "Huế", "Việt Nam", "HUI", "Phú Bài International Airport" },
                    { new Guid("e1d9fb2f-641a-4ac9-a9e1-8d9e1cf72884"), "Vinh", "Việt Nam", "VII", "Vinh Airport" },
                    { new Guid("f81bc92c-4d24-429b-b007-8ee188a2f689"), "Đà Nẵng", "Việt Nam", "DAD", "Đà Nẵng International Airport" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Flights_AircraftId",
                table: "Flights",
                column: "AircraftId");

            migrationBuilder.CreateIndex(
                name: "IX_Flights_AirlineId",
                table: "Flights",
                column: "AirlineId");

            migrationBuilder.CreateIndex(
                name: "IX_Flights_ArrivalAirportId",
                table: "Flights",
                column: "ArrivalAirportId");

            migrationBuilder.CreateIndex(
                name: "IX_Flights_DepartureAirportId",
                table: "Flights",
                column: "DepartureAirportId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Flights");

            migrationBuilder.DropTable(
                name: "Hangfires");

            migrationBuilder.DropTable(
                name: "Aircrafts");

            migrationBuilder.DropTable(
                name: "Airlines");

            migrationBuilder.DropTable(
                name: "Airports");
        }
    }
}
