namespace Flight.Services.DTOs.AirportDTO;

public class GetAirportDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string IATA { get; set; }
}