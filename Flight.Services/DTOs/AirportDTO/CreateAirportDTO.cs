using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.AirportDTO;

public class CreateAirportDTO
{
    [MinLength(3)]
    [MaxLength(200)]
    [Required(ErrorMessage = "Name is required")]
    [DataType(DataType.Text)]
    public string Name { get; set; }

    [MinLength(3)]
    [MaxLength(50)]
    [DataType(DataType.Text)]
    [Required(ErrorMessage = "City is required")]
    public string City { get; set; }

    [MinLength(3)]
    [MaxLength(50)]
    [DataType(DataType.Text)]
    [Required(ErrorMessage = "Country is required")]
    public string Country { get; set; }

    [MinLength(3)]
    [MaxLength(3)]
    [DataType(DataType.Text)]
    [Required(ErrorMessage = "IATA is required")]
    public string IATA { get; set; }
}