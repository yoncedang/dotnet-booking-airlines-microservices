namespace Flight.Services.DTOs.AirportDTO;

public class ListDeleteAirportID
{
    public List<Guid> Id { get; set; }
}