using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.AirportDTO;

public class UpdateAirportDTO
{
    [MinLength(3)]
    [MaxLength(200)]
    [DataType(DataType.Text)]
    public string? Name { get; set; }

    [MinLength(3)]
    [MaxLength(50)]
    [DataType(DataType.Text)]
    public string? City { get; set; }

    [MinLength(3)]
    [MaxLength(50)]
    [DataType(DataType.Text)]
    public string? Country { get; set; }

    [MinLength(3)]
    [MaxLength(3)]
    [DataType(DataType.Text)]
    public string? IATA { get; set; }
}