using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.AircraftDTO;

public class CreateAircraftDTO
{
    [Required(ErrorMessage = "Name is required")]
    [DataType(DataType.Text)]
    [StringLength(50, ErrorMessage = "Name is too long")]
    [MinLength(3, ErrorMessage = "Name is too short")]
    [Display(Name = "Airbus")]
    [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Name is invalid")]

    public string Name { get; set; }

    [Required(ErrorMessage = "Model is required")]
    [DataType(DataType.Text)]
    [StringLength(10, ErrorMessage = "Model is too long")]
    [MinLength(3, ErrorMessage = "Model is too short")]
    [Display(Name = "A320")]

    // Not allow special characters
    [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Model is invalid")]


    public string Model { get; set; }


    [Required(ErrorMessage = "TotalSeats is required")]
    [Range(72, 350, ErrorMessage = "TotalSeats is invalid")]
    [Display(Name = "TotalSeats")]
    [DataType(DataType.Text)]

    public int TotalSeats { get; set; }
}