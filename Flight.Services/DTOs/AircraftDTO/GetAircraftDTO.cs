namespace Flight.Services.DTOs.AircraftDTO;

public class GetAircraftDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Model { get; set; }
    public int TotalSeats { get; set; }
    public bool Active { get; set; }
}