using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.AircraftDTO;

public class UpdateAircraftDTO
{
    [DataType(DataType.Text)]
    [StringLength(50, ErrorMessage = "Name is too long")]
    [MinLength(3, ErrorMessage = "Name is too short")]
    [Display(Name = "Airbus")]
    [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Name is invalid")]
    public string? Name { get; set; }

    [DataType(DataType.Text)]
    [StringLength(10, ErrorMessage = "Model is too long")]
    [MinLength(3, ErrorMessage = "Model is too short")]
    [Display(Name = "A320")]
    [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Model is invalid")]
    public string? Model { get; set; }

    [Range(72, 350, ErrorMessage = "TotalSeats is invalid")]
    [Display(Name = "TotalSeats")]
    [DataType(DataType.Text)]

    public int? TotalSeats { get; set; }

    [DataType(DataType.Text)]
    [Display(Name = "Active")]
    [RegularExpression("^(true|false)$", ErrorMessage = "Active is invalid")]

    // Just allow true or false
    public bool? Active { get; set; }
}