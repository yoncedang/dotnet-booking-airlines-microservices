namespace Flight.Services.DTOs.AirlinesDTO;

public class GetAirlinesDTO
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string IATA { get; set; }
}