using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.FlightDTO;

public class CreateFlightDTO
{
    [Required(ErrorMessage = "Flight code is required")]
    [Range(100, 99999, ErrorMessage = "Flight code must be between 100 and 99999")]
    public int FlightCode { get; set; }


    [Required(ErrorMessage = "Departure airport is required")]
    public Guid DepartureAirportId { get; set; }


    [Required(ErrorMessage = "Arrival airport is required")]
    public Guid ArrivalAirportId { get; set; }


    [Required(ErrorMessage = "Departure time is required")]
    public DateTimeOffset DepartureTime { get; set; }


    [Required(ErrorMessage = "Arrival time is required")]
    public DateTimeOffset ArrivalTime { get; set; }


    [DataType(DataType.Currency)]
    [Required(ErrorMessage = "Price is required")]
    [Range(100, 10000, ErrorMessage = "Price must be between 100 and 10000")]
    public double Price { get; set; }


    [Required(ErrorMessage = "Aircraft is required")]
    public Guid AircraftId { get; set; }
    // public string FlightDuration { get; set; }
}