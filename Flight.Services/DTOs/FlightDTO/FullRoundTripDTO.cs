namespace Flight.Services.DTOs.FlightDTO;

public class FullRoundTripDTO
{
    public IEnumerable<ResponseFullFlightDTO> FirstFlight { get; set; }
    public IEnumerable<ResponseFullFlightDTO> SecondFlight { get; set; }
}