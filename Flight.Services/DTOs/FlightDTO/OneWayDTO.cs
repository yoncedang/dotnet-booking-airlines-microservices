using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.FlightDTO;

public class OneWayDTO
{
    [Required(ErrorMessage = "Quantity tickets is required")]
    [Range(1, 350, ErrorMessage = "Quantity tickets must be between 1 and 350")]
    [Display(Name = "Quantity tickets")]
    [DataType(DataType.Text)]
    public int QuantityTickets { get; set; }

    [Required(ErrorMessage = "Date is required")]
    [Display(Name = "Date")]
    [DataType(DataType.Date)]

    public DateTime Date { get; set; }

    [Required(ErrorMessage = "Departure airport is required")]
    [Display(Name = "Departure airport")]
    public Guid DepartureAirportId { get; set; }


    [Required(ErrorMessage = "Arrival airport is required")]
    [Display(Name = "Arrival airport")]
    public Guid ArrivalAirportId { get; set; }
}