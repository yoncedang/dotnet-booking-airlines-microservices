using Flight.Services.DTOs.AircraftDTO;
using Flight.Services.DTOs.AirlinesDTO;
using Flight.Services.DTOs.AirportDTO;

namespace Flight.Services.DTOs.FlightDTO;

public class ResponseFullFlightDTO
{
    public Guid Id { get; set; }
    public string FlightCode { get; set; }
    public DateTimeOffset DepartureTime { get; set; }
    public DateTimeOffset ArrivalTime { get; set; }
    public int AvailableSeats { get; set; }
    public double Price { get; set; }
    public bool Status { get; set; }
    public string FlightDuration { get; set; }
    public GetAirportDTO DepartureAirport { get; set; }
    public GetAirportDTO ArrivalAirport { get; set; }
    public GetAirlinesDTO Airline { get; set; }
    public GetAircraftDTO Aircraft { get; set; }
}