using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.FlightDTO;

public class UpdateFlightDTO
{
    public Guid? DepartureAirportId { get; set; }
    public Guid? ArrivalAirportId { get; set; }

    public DateTimeOffset? DepartureTime { get; set; }
    public DateTimeOffset? ArrivalTime { get; set; }

    [DataType(DataType.Currency)]
    [Range(100, 10000, ErrorMessage = "Price must be between 100 and 10000")]
    public double? Price { get; set; }

    public Guid? AircraftId { get; set; }
}