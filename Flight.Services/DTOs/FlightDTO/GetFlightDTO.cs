namespace Flight.Services.DTOs.FlightDTO;

public class GetFlightDTO
{
    public Guid Id { get; set; }
    public string FlightCode { get; set; }
    public Guid DepartureAirportId { get; set; }
    public Guid ArrivalAirportId { get; set; }
    public DateTimeOffset DepartureTime { get; set; }
    public DateTimeOffset ArrivalTime { get; set; }
    public int AvailableSeats { get; set; }
    public double Price { get; set; }
    public bool Status { get; set; }
    public Guid AirlineId { get; set; }
    public Guid AircraftId { get; set; }
    public string FlightDuration { get; set; }
}