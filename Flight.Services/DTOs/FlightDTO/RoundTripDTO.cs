using System.ComponentModel.DataAnnotations;

namespace Flight.Services.DTOs.FlightDTO;

public class RoundTripDTO
{
    [Required(ErrorMessage = "Quantity tickets is required")]
    [Range(1, 350, ErrorMessage = "Quantity tickets must be between 1 and 350")]
    // [RegularExpression(@"^\d+$", ErrorMessage = "Quantity tickets must be a number")]
    [Display(Name = "Quantity tickets")]
    [DataType(DataType.Text)]
    public int QuantityTickets { get; set; }

    [Required(ErrorMessage = "Date Start is required")]
    [Display(Name = "Date")]
    [DataType(DataType.Date)]

    public DateTime DateStart { get; set; }

    [Required(ErrorMessage = "Date Back is required")]
    [Display(Name = "Date")]
    [DataType(DataType.Date)]

    public DateTime DateBack { get; set; }

    [Required(ErrorMessage = "Departure airport is required")]
    [Display(Name = "Departure airport")]
    public Guid DepartureAirportId { get; set; }


    [Required(ErrorMessage = "Arrival airport is required")]
    [Display(Name = "Arrival airport")]
    public Guid ArrivalAirportId { get; set; }
}